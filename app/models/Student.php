<?php

class Student extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id_of_student;

    /**
     *
     * @var string
     */
    public $id_of_person;

    /**
     *
     * @var integer
     */
    public $id_of_room;

    /**
     *
     * @var integer
     */
    public $room_order;

    /**
     *
     * @var string
     */
    public $description;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'student';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Student[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Student
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
