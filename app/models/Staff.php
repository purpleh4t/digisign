<?php

use Phalcon\Mvc\Model\Validator\Email as Email;

class Staff extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id_of_staff;

    /**
     *
     * @var string
     */
    public $id_of_person;

    /**
     *
     * @var string
     */
    public $id_of_room;

    /**
     *
     * @var integer
     */
    public $room_order;

    /**
     *
     * @var string
     */
    public $description;

    /**
     *
     * @var string
     */
    public $email;

    /**
     *
     * @var string
     */
    public $encrypted_password;

    /**
     *
     * @var string
     */
    public $salt;

    /**
     *
     * @var integer
     */
    public $is_admin;

    /**
     * Validations and business logic
     *
     * @return boolean
     */
    public function validation()
    {
        $this->validate(
            new Email(
                array(
                    'field'    => 'email',
                    'required' => true,
                )
            )
        );

        if ($this->validationHasFailed() == true) {
            return false;
        }

        return true;
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'staff';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Staff[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Staff
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
