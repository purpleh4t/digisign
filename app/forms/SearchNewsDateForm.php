<?php

/**
 * Created by PhpStorm.
 * User: HermawanRahmatHidaya
 * Date: 20/09/2016
 * Time: 14.16
 */

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Date;
use Phalcon\Forms\Element\Hidden;

class SearchNewsDateForm extends Form{
    public function initialize($entity = null, $options = null){
        $dateFrom = new Date('dateFrom');
        $dateFrom->setLabel('From');
        $this->add($dateFrom);

        $dateTo = new Date('dateTo');
        $dateTo->setLabel("To");
        $this->add($dateTo);

        $room = new Hidden("room");
        $this->add($room);
    }
}