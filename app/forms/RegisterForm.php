<?php

/**
 * Created by PhpStorm.
 * User: HermawanRahmatHidaya
 * Date: 19/08/2016
 * Time: 08.59
 */

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Select;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;

class RegisterForm extends Form{
    public function initialize($entity = null, $options = null){
        // id
        $name = new Text('id_of_person');
        $name->setLabel('Your Identity Number (NIP/NIM)');
        $name->setFilters(array('striptags', 'string'));
        $name->addValidators(array(
            new PresenceOf(array(
                'message' => 'ID is required'
            ))
        ));
        $this->add($name);

        // Name
        $name = new Text('name');
        $name->setLabel('Your Full Name');
        $name->setFilters(array('striptags', 'string'));
        $name->addValidators(array(
            new PresenceOf(array(
                'message' => 'Name is required'
            ))
        ));
        $this->add($name);

        // Type
        $type = new Select("type", array(
            "Lecturer"=>"Lecturer",
            "Staff"=>"Staff",
            "Student"=>"Student"
        ));
        $type->setLabel("Account Type");
        $this->add($type);

        // Description
        $description = new Text('description');
        $description->setLabel('Description');
        $description->setFilters(array('striptags', 'string'));
        $description->setAttribute("readonly","readonly");
        $description->addValidators(array(
            new PresenceOf(array(
                'message' => 'Account description is required'
            ))
        ));
        $this->add($description);

        // RFID
        $rfid = new Text('rfid');
        $rfid->setLabel('RFID');
        $rfid->addValidators(array(
            new PresenceOf(array(
                'message' => 'Please enter your RFID card'
            ))
        ));
        $this->add($rfid);

        // Email
        $email = new Text('email');
        $email->setLabel('Email');
        $email->setFilters('email');
        $email->addValidators(array(
            new PresenceOf(array(
                'message' => 'Please enter your email address'
            )),
            new Email(array(
                'message' => 'Email is not valid'
            ))
        ));
        $this->add($email);

        // Password
        $password = new Password('password');
        $password->setLabel('Password');
        $password->addValidators(array(
            new PresenceOf(array(
                'message' => 'Password is required'
            ))
        ));
        $this->add($password);
    }
}