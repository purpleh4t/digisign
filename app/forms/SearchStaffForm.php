<?php

/**
 * Created by PhpStorm.
 * User: HermawanRahmatHidaya
 * Date: 30/09/2016
 * Time: 23.01
 */

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Hidden;
use \Phalcon\Forms\Element\Select;
class SearchStaffForm extends Form{
    public function initialize($entity = null, $options = array()){
        $keywords = new Text("keywords");
        $keywords->setLabel("Keywords");
        $this->add($keywords);

        $hidden = new Hidden("id_of_room");
        $hidden->setDefault($options["id_of_room"]);
        $this->add($hidden);

        $hidden = new Hidden("type");
        $hidden->setDefault($options["type"]);
        $this->add($hidden);

    }
}