{{ content() }}
<div class="page-header">
    <h2>Register for DigiSign</h2>
</div>

{{ form('register', 'id': 'registerForm', 'onbeforesubmit': 'return false') }}
<fieldset>
    <div class="control-group">
        {{ form.label('id_of_person', ['class': 'control-label']) }}
        <div class="controls">
            {{ form.render('id_of_person', ['class': 'form-control']) }}
            <p class="help-block">(required)</p>
            <div class="alert alert-warning" id="id_of_person_alert">
                <strong>Warning!</strong> Please enter your identity number
            </div>
        </div>
    </div>

    <div class="control-group">
        {{ form.label('name', ['class': 'control-label']) }}
        <div class="controls">
            {{ form.render('name', ['class': 'form-control']) }}
            <p class="help-block">(required)</p>
            <div class="alert alert-warning" id="name_alert">
                <strong>Warning!</strong> Please enter your full name
            </div>
        </div>
    </div>

    <div class="control-group">
        {{ form.label('type', ['class': 'control-label']) }}
        <div class="controls">
            {{ form.render('type', ['class': 'form-control', 'onchange' :'staffChecks();']) }}
            <p class="help-block">(required)</p>
            <div class="alert alert-warning" id="type_alert">
                <strong>Warning!</strong> Please choose account type
            </div>
        </div>
    </div>

    <div id="staffForm">
        <div class="control-group">
            {{ form.label('email', ['class': 'control-label']) }}
            <div class="controls">
                {{ form.render('email', ['class': 'form-control']) }}
                <p class="help-block">(required)</p>
                <div class="alert alert-warning" id="email_alert">
                    <strong>Warning!</strong> Please enter your email address
                </div>
            </div>
        </div>

        <div class="control-group">
            {{ form.label('password', ['class': 'control-label']) }}
            <div class="controls">
                {{ form.render('password', ['class': 'form-control']) }}
                <p class="help-block">(minimum 8 characters)</p>
                <div class="alert alert-warning" id="password_alert">
                    <strong>Warning!</strong> Please provide a valid password
                </div>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="repeatPassword">Repeat Password</label>
            <div class="controls">
                {{ password_field('repeatPassword', 'class': 'form-control') }}
                <p class="help-block">(required)</p>
                <div class="alert alert-warning" id="repeatPassword_alert">
                    <strong>Warning!</strong> The password does not match
                </div>
            </div>
        </div>

        {{ roomElements.getRoomSelection() }}
        {{ javascript_include('js/roomselector.js') }}
    </div>

    <div class="control-group">
        {{ form.label('description', ['class': 'control-label']) }}
        <div class="controls">
            {{ form.render('description', ['class': 'form-control']) }}
            <p class="help-block">(required)</p>
            <div class="alert alert-warning" id="description_alert">
                <strong>Warning!</strong> Please describe about your position
            </div>
        </div>
    </div>

    <div class="control-group">
        {{ form.label('rfid', ['class': 'control-label']) }}
        <div class="controls">
            {{ form.render('rfid', ['class': 'form-control']) }}
            <p class="help-block">(required)</p>
            <div class="alert alert-warning" id="rfid_alert">
                <strong>Warning!</strong> Please enter your RFID tag
            </div>
        </div>
    </div>

    <div class="form-actions">
        {{ submit_button('Register', 'class': 'btn btn-primary', 'onclick': 'return FormElement.validate();') }}
        <p class="help-block">By signing up, you accept terms of use and privacy policy.</p>
    </div>
</fieldset>
</form>

{{ javascript_include('js/register/register.js') }}