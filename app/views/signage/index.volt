{{ content() }}

{{ stylesheet_link("css/rooms/main.css") }}

<div id="roomSelectorDiv">
    <div class="well">
        <h1>Choose a Room Signage</h1>
        {{ form('signage/show', 'method':'GET') }}
        <fieldset>
            {{ roomElements.getRoomSelection() }}
            {{ javascript_include('js/roomselector.js') }}
            <div class="form-actions">
                {{ submit_button('Select', 'id': 'selectButton', 'class': 'btn btn-info pull-right') }}
            </div>
        </fieldset>
        </form>
    </div>
</div>