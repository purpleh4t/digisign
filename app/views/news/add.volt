{{ content() }}

{{ headerElements.getTabs() }}

<h1>Create a News</h1>

{{ form('news/add', 'id': 'addNewsForm', 'method':'POST') }}
<fieldset>
    <div class="control-group">
        {{ form.label('title', ['class': 'control-label']) }}
        <div class="controls">
            {{ form.render('title', ['class': 'form-control']) }}
        </div>
    </div>

    <div class="control-group">
        {{ form.label('content', ['class': 'control-label']) }}
        <div class="controls">
            {{ form.render('content', ['class': 'form-control']) }}
        </div>
    </div>

    <div class="control-group">
        {{ form.label('image', ['class': 'control-label']) }}
        <div class="controls">
            {{ form.render('image', ['class': 'form-control']) }}
        </div>
    </div>

    <div class="control-group">
        {{ form.label('startTime', ['class': 'control-label']) }}
        <div class="controls">
            {{ form.render('startTime', ['class': 'form-control datepicker ']) }}
        </div>
    </div>

    <div class="control-group">
        {{ form.label('endTime', ['class': 'control-label']) }}
        <div class="controls">
            {{ form.render('endTime', ['class': 'form-control datepicker']) }}
        </div>
    </div>

    {{ form.render("room") }}

    <div class="form-actions" style="margin-top:10px;">
        {{ submit_button('Save', 'class': 'btn btn-primary') }}
        <p class="help-block">Please double check your data before save</p>
    </div>
</fieldset>