{{ content() }}
<div class="jumbotron">
    <h1>Welcome to <b>SmartLab</b></h1>
    <p><b>SmartLab</b> is a revolutionary portal to control rooms and manage digital signage in Universitas Gadjah Mada</p>
    <p>{{ link_to('register', 'Register now &raquo;', 'class': 'btn btn-primary btn-large btn-success') }}</p>
</div>

