{{ content() }}
<div class="row">

    <div class="col-md-6">
        <div class="page-header">
            <h2>Log In</h2>
        </div>
        {{ form('session/start', 'role': 'form') }}
        <fieldset>
            <div class="form-group">
                <label for="email">Email</label>
                <div class="controls">
                    {{ text_field('email', 'class': "form-control") }}
                </div>
            </div>
            <div class="form-group">
                <label for="password">DigiSign Password</label>
                <div class="controls">
                    {{ password_field('password', 'class': "form-control") }}
                </div>
            </div>
            <div class="form-group">
                {{ submit_button('Login', 'class': 'btn btn-primary btn-large') }}
            </div>
        </fieldset>
        </form>
    </div>

    <div class="col-md-6">

        <div class="page-header">
            <h2>Don't have an account yet?</h2>
        </div>

        <p>Create an account offers the following advantages:</p>
        <ul>
            <li>Rooms access in Universitas Gadjah Mada</li>
            <li>Monitoring people presence in your room</li>
            <li>News posting direct to your room</li>
            <li>Automated presence every room visited</li>
        </ul>

        <div class="clearfix center">
            {{ link_to('register', 'Sign Up', 'class': 'btn btn-primary btn-large btn-success') }}
        </div>
    </div>
</div>

{{ javascript_include("js/signin/signin.js") }}
