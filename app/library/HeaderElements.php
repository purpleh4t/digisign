<?php
/**
 * Created by PhpStorm.
 * User: HermawanRahmatHidaya
 * Date: 18/08/2016
 * Time: 23.36
 */

use Phalcon\Mvc\User\Component;

class HeaderElements extends Component{
    private $_headerMenu = array(
        'navbar-left' => array(
            'index' => array(
                'caption' => 'Home',
                'action' => 'index'
            ),
            'dashboard' => array(
                'caption' => 'Dashboard',
                'action' => 'index'
            ),
            'signage' => array(
                'caption' => 'Signage',
                'action' => 'index'
            ),
            'about' => array(
                'caption' => 'About',
                'action' => 'index'
            ),
            'contact' => array(
                'caption' => 'Contact',
                'action' => 'index'
            ),
        ),
        'navbar-right' => array(
            'session' => array(
                'caption' => 'Log In/Sign Up',
                'action' => 'index'
            ),
        )
    );

    private $_tabs = array(
        'Manage Rooms' => array(
            'controller' => 'rooms',
            'action' => 'index'
        ),
        'Manage News' => array(
            'controller' => 'news',
            'action' => 'index'
        ),
        'Manage Users' => array(
            'controller' => 'users',
            'action' => 'index'
        ),
        'Activity Report' => array(
            'controller' => 'activities',
            'action' => 'profile'
        )
    );

    /**
     * Builds header menu with left and right items
     *
     * @return string
     */
    public function getMenu()
    {
        $auth = $this->session->get('authentication');
        if ($auth) {
            $this->_headerMenu['navbar-right']['session'] = array(
                'caption' => $auth["name"] . " (Log Out)",
                'action' => 'end'
            );
        } else {
            unset($this->_headerMenu['navbar-left']['dashboard']);
        }

        $controllerName = $this->router->getControllerName();
        foreach ($this->_headerMenu as $position => $menu) {
            echo '<div class="nav-collapse">';
            echo '<ul class="nav navbar-nav ', $position, '">';
            foreach ($menu as $controller => $option) {
                if ($controllerName == $controller) {
                    echo '<li class="active">';
                } else {
                    echo '<li>';
                }
                echo $this->tag->linkTo($controller . '/' . $option['action'], $option['caption']);
                echo '</li>';
            }
            echo '</ul>';
            echo '</div>';
        }

    }

    /**
     * Returns menu tabs
     */
    public function getTabs()
    {
        $controllerName = $this->view->getControllerName();
        $actionName = $this->view->getActionName();
        echo '<ul class="nav nav-tabs">';
        foreach ($this->_tabs as $caption => $option) {
            if ($option['controller'] == $controllerName) {
                echo '<li class="active">';
            } else {
                echo '<li>';
            }
            echo $this->tag->linkTo($option['controller'] . '/' . $option['action'], $caption), '</li>';
        }
        echo '</ul>';
    }
}