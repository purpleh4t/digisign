<?php

/**
 * Created by PhpStorm.
 * User: HermawanRahmatHidaya
 * Date: 09/09/2016
 * Time: 10.17
 */
use Phalcon\Mvc\User\Component;

class RoomElements extends Component
{
    private $_roomElements = array(
        "faculty"=>null,
        "department"=>null,
        "building"=>null,
        "room"=>null
    );

    public function getRoomSelection(){
        $rooms = Room::find();

        echo "<script>\nvar rooms = [];\n";
        foreach ($rooms as $room){
            $building = Building::findFirst(array("id_of_building = '$room->id_of_building'"));

            echo "rooms.push({";
            echo "buildingID:'$building->id_of_building',";
            echo "buildingName:'$building->name',";
            echo "roomID:'$room->id_of_room',";
            echo "roomName:'$room->name',";
            echo "roomCode: '$room->code_room',";
            echo "roomFloor: '$room->floor'";
            echo "});\n";
        }

        $buildings = Building::find();
        echo "var buildings = [];\n";
        foreach ($buildings as $building){
            $department = Department::findFirst(array("id_of_department = '$building->id_of_department'"));

            echo "buildings.push({";
            echo "departmentID:'$department->id_of_department',";
            echo "departmentName:'$department->name',";
            echo "buildingID:'$building->id_of_building',";
            echo "buildingName:'$building->name'";
            echo "});\n";
        }

        $departments = Department::find();
        echo "var departments = [];\n";
        foreach ($departments as $department){
            $faculty = Faculty::findFirst(array("id_of_faculty = '$department->id_of_faculty'"));

            echo "departments.push({";
            echo "facultyID:'$faculty->id_of_faculty',";
            echo "facultyName:'$faculty->name',";
            echo "departmentID:'$department->id_of_department',";
            echo "departmentName:'$department->name'";
            echo "});\n";
        }

        $faculties = Faculty::find();
        echo "var faculties = [];\n";
        foreach ($faculties as $faculty){
            echo "faculties.push({";
            echo "facultyID:'$faculty->id_of_faculty',";
            echo "facultyName:'$faculty->name',";
            echo "});\n";
        }
        echo "</script>";
        echo "\n";

        echo "<div class=\"control-group\">\n";
        echo "<label for=\"faculty\" class=\"control-label\">Faculty</label>\n";
        echo "<div class=\"controls\">\n";
        echo "<select id=\"faculty\" name=\"faculty\" class=\"form-control\" onchange='facultyCheck();'>";
        echo "<option disabled selected value> -- Select from options below -- </option>";
        echo "</select>";
        echo "<p class=\"help-block\">(required)</p>";
        echo "<div class=\"alert alert-warning\" id=\"type_alert\" style=\"display: none;\">\n";
        echo "<strong>Warning!</strong> Please choose a faculty";
        echo "</div>";
        echo "</div>\n";
        echo "</div>\n";

        echo "<div id='departmentGroup' class=\"control-group\" style=\"display: none;\">\n";
        echo "<label for=\"department\" class=\"control-label\">Department</label>\n";
        echo "<div class=\"controls\">\n";
        echo "<select id=\"department\" name=\"department\" class=\"form-control\" onchange='departmentCheck();'>";
        echo "</select>";
        echo "<p class=\"help-block\">(required)</p>";
        echo "<div class=\"alert alert-warning\" id=\"type_alert\" style=\"display: none;\">\n";
        echo "<strong>Warning!</strong> Please choose a department";
        echo "</div>";
        echo "</div>\n";
        echo "</div>\n";

        echo "<div id='buildingGroup' class=\"control-group\" style=\"display: none;\">\n";
        echo "<label for=\"building\" class=\"control-label\">Buiding</label>\n";
        echo "<div class=\"controls\">\n";
        echo "<select id=\"building\" name=\"faculty\" class=\"form-control\" onchange='buildingCheck();'>";
        echo "</select>";
        echo "<p class=\"help-block\">(required)</p>";
        echo "<div class=\"alert alert-warning\" id=\"type_alert\" style=\"display: none;\">\n";
        echo "<strong>Warning!</strong> Please choose a building";
        echo "</div>";
        echo "</div>\n";
        echo "</div>\n";

        echo "<div id='roomGroup' class=\"control-group\" style=\"display: none;\">\n";
        echo "<label for=\"room\" class=\"control-label\">Room</label>\n";
        echo "<div class=\"controls\">\n";
        echo "<select id=\"room\" name=\"room\" class=\"form-control\">";
        echo "</select>";
        echo "<p class=\"help-block\">(required)</p>";
        echo "<div class=\"alert alert-warning\" id=\"type_alert\" style=\"display: none;\">\n";
        echo "<strong>Warning!</strong> Please choose a building";
        echo "</div>";
        echo "</div>\n";
        echo "</div>\n";
    }
}