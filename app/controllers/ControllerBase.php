<?php

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{
    public function initialize(){
        date_default_timezone_set('Asia/Jakarta');
        $this->tag->prependTitle('SmartLab | ');

    }

    protected function forward($uri)
    {
        $uriParts = explode('/', $uri);
        $params = array_slice($uriParts, 2);
        return $this->dispatcher->forward(
            array(
                'controller' => $uriParts[0],
                'action' => $uriParts[1],
                'params' => $params
            )
        );
    }

    protected function mustLogin(){
        if($this->session->get("authentication")!=null){
            return true;
        }else{
            $this->flash->error("You are not authenticated");
            $this->forward("session//");
            return false;
        }
    }

    protected function interval($from, $to){
        $seconds = strtotime("$to") - strtotime("$from");
        $output = $seconds." seconds";
        if($seconds>60){
            $minutes = round($seconds/60, 0, PHP_ROUND_HALF_DOWN);
            $output = $minutes." minutes";
            if($minutes>60){
                $hours = round($minutes/60, 0, PHP_ROUND_HALF_DOWN);
                $output = $hours." hours";
            }
        }
        return $output;
    }

    protected function isToday($datetime){
        if(date('Ymd') == date('Ymd', strtotime($datetime))){
            return true;
        }else{
            return false;
        }
    }
}
