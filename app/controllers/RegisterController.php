<?php

class RegisterController extends ControllerBase
{
    public function initialize(){
        $this->tag->setTitle('Register');
        $this->view->roomElements = new RoomElements();
        parent::initialize();
    }

    public function indexAction(){
        $form = new RegisterForm();
        if($this->request->isPost()){
            // data acquisition form $_POST data
            $id_of_person = $this->request->getPost("id_of_person");
            $name = $this->request->getPost("name");
            $type = $this->request->getPost("type");
            $description = $this->request->getPost("description");
            $rfid =  $this->request->getPost("rfid");

            $arrayContent = array(
                "id_of_person"=>$id_of_person,
                "name"=>$name,
                "type"=>$type,
                "description"=>$description,
                "rfid"=>$rfid
            );

            if($type == "Lecturer" || $type == "Staff"){
                $email = $this->request->getPost('email');
                $password = $this->request->getPost("password");
                $repeatPassword = $this->request->getPost("repeatPassword");
                $room = $this->request->getPost("room");
                $arrayContent["id_of_room"] = $room;
                $arrayContent["email"] = $email;
                $arrayContent["password"] = $password;
                $arrayContent["repeatPassword"] = $repeatPassword;
            }

            // Save to database

            // Check uniqueness Person data
            $isPersonExist = Person::findFirst(array("id_of_person = :id_of_person:", "bind"=>array("id_of_person"=>$arrayContent["id_of_person"])));
            $makeStaff = false;
            $isStaffRegistered = true;
            if($arrayContent["type"] == "Lecturer" || $arrayContent["type"] == "Staff"){
                $makeStaff = true;
                // Check uniqueness Staff data
                $isStaffRegistered = Staff::findFirst(array("email = :email: OR id_of_person = :id_of_person:",
                    "bind"=>array("email"=>$arrayContent["email"], "id_of_person"=>$arrayContent["id_of_person"])));
            }else{
                $makeStaff = false;
            }

            if(!$isPersonExist){
                // Create a new Person
                $person = new Person();
                $person->id_of_person = $arrayContent["id_of_person"];
                $person->name = $arrayContent["name"];
                $person->type = $arrayContent["type"];
                $person->description = $arrayContent["description"];
                $person->image_link = " ";
                $person->RFID = $arrayContent["rfid"];

                // Create a new Staff if lecturer or staff
                if($makeStaff){
                    if(!$isStaffRegistered){
                        // Make hashcode and salt string
                        $salt = sha1(rand());
                        $salt = substr($salt, 0, 10);
                        $encrypted_password = base64_encode(sha1($arrayContent["password"] . $salt, true) . $salt);
                        $staff = new Staff();
                        $staff->id_of_person = $person->id_of_person;
                        $staff->id_of_room = $arrayContent["id_of_room"];
                        $staff->email = $arrayContent["email"];
                        $staff->encrypted_password = $encrypted_password;
                        $staff->salt = $salt;
                        $staff->is_admin = 0;

                        $person->create();
                        $staff->create();

                        $this->flash->success("Registered successfully!");
                        return $this->forward('session/index');
                    }else{
                        $this->flash->error("Email or ID has been taken");
                    }
                }else{
                    $person->create();

                    $this->flash->success("Registered successfully!");
                    return $this->forward('session/index');
                }
            }else{
                $this->flash->error("This ID has been taken");
            }
        }
        $this->view->form = $form;
    }

}

