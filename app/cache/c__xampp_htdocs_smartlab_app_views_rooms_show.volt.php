<?php echo $this->getContent(); ?>

<?php echo $this->headerElements->getTabs(); ?>

<h1><?php echo $title; ?></h1>

<div id="lecturerDiv">
    <h3>Lecturers</h3>
    <?php $v30792628861iterator = $lecturers; $v30792628861incr = 0; $v30792628861loop = new stdClass(); $v30792628861loop->length = count($v30792628861iterator); $v30792628861loop->index = 1; $v30792628861loop->index0 = 1; $v30792628861loop->revindex = $v30792628861loop->length; $v30792628861loop->revindex0 = $v30792628861loop->length - 1; ?><?php foreach ($v30792628861iterator as $lecturer) { ?><?php $v30792628861loop->first = ($v30792628861incr == 0); $v30792628861loop->index = $v30792628861incr + 1; $v30792628861loop->index0 = $v30792628861incr; $v30792628861loop->revindex = $v30792628861loop->length - $v30792628861incr; $v30792628861loop->revindex0 = $v30792628861loop->length - ($v30792628861incr + 1); $v30792628861loop->last = ($v30792628861incr == ($v30792628861loop->length - 1)); ?>
        <?php if ($v30792628861loop->first) { ?>
            <table class="table">
            <tr>
                <th>Num</th>
                <th>Name</th>
                <th>Status</th>
                <th>Last Update</th>
                <?php if ($isValidToAdd && $this->length($lecturers) > 1) { ?>
                    <th>Order</th>
                <?php } ?>
            </tr>
        <?php } ?>
        <tr>
            <td><?php echo $lecturer->room_order; ?></td>
            <td><?php echo $lecturer->name; ?></td>
            <td><?php echo $lecturer->status; ?></td>
            <td><?php echo $lecturer->last_update; ?></td>
            <?php if ($this->length($lecturers) > 1) { ?>
            <td>
                <?php if ($isValidToAdd) { ?>
                    <?php if ($v30792628861loop->first) { ?>
                        <div class="col-xs-1">
                            <?php echo $this->tag->form(array('rooms/modify', array('method' => 'POST'))); ?>
                            <fieldset>
                                <input type="hidden" name="action" value="decrease"/>
                                <input type="hidden" name="id_of_person" value="<?php echo $lecturer->id_of_person; ?>"/>
                                <input type="hidden" name="id_of_room" value="<?php echo $id_of_room; ?>"/>
                                <div class="form-actions">
                                    <?php echo $this->tag->submitButton(array('DOWN', 'class' => 'btn btn-danger btn-xs')); ?>
                                </div>
                            </fieldset>
                            </form>
                        </div>
                    <?php } elseif ($v30792628861loop->last) { ?>
                        <div class="col-xs-1">
                            <?php echo $this->tag->form(array('rooms/modify', array('method' => 'POST'))); ?>
                            <fieldset>
                                <input type="hidden" name="action" value="increase"/>
                                <input type="hidden" name="id_of_person" value="<?php echo $lecturer->id_of_person; ?>"/>
                                <input type="hidden" name="id_of_room" value="<?php echo $id_of_room; ?>"/>
                                <div class="form-actions">
                                    <?php echo $this->tag->submitButton(array('UP', 'class' => 'btn btn-primary btn-xs')); ?>
                                </div>
                            </fieldset>
                            </form>
                        </div>
                    <?php } else { ?>
                        <div class="col-xs-1">
                            <?php echo $this->tag->form(array('rooms/modify', array('method' => 'POST'))); ?>
                            <fieldset>
                                <input type="hidden" name="action" value="increase"/>
                                <input type="hidden" name="id_of_person" value="<?php echo $lecturer->id_of_person; ?>"/>
                                <input type="hidden" name="id_of_room" value="<?php echo $id_of_room; ?>"/>
                                <div class="form-actions">
                                    <?php echo $this->tag->submitButton(array('UP', 'class' => 'btn btn-primary btn-xs')); ?>
                                </div>
                            </fieldset>
                            </form>
                        </div>
                        <div class="col-xs-1">
                            <?php echo $this->tag->form(array('rooms/modify', array('method' => 'POST'))); ?>
                            <fieldset>
                                <input type="hidden" name="action" value="decrease"/>
                                <input type="hidden" name="id_of_person" value="<?php echo $lecturer->id_of_person; ?>"/>
                                <input type="hidden" name="id_of_room" value="<?php echo $id_of_room; ?>"/>
                                <div class="form-actions">
                                    <?php echo $this->tag->submitButton(array('DOWN', 'class' => 'btn btn-danger btn-xs')); ?>
                                </div>
                            </fieldset>
                            </form>
                        </div>
                    <?php } ?>
                <?php } ?>
            </td>
            <?php } ?>
        </tr>
        <?php if ($v30792628861loop->last) { ?>
            </table>
        <?php } ?>
    <?php $v30792628861incr++; } ?>

    <?php if ($isValidToAdd) { ?>
        <div id="buttons" style="text-align: right">
            <a href="add?id_of_room=<?php echo $id_of_room; ?>&type=Lecturer" class="btn btn-primary smallMargin" id="addLecturerButton">ADD</a>
        </div>
        <br>
    <?php } ?>
</div>

<div id="staffDiv">
    <h3>Staffs</h3>
    <?php $v30792628861iterator = $staffs; $v30792628861incr = 0; $v30792628861loop = new stdClass(); $v30792628861loop->length = count($v30792628861iterator); $v30792628861loop->index = 1; $v30792628861loop->index0 = 1; $v30792628861loop->revindex = $v30792628861loop->length; $v30792628861loop->revindex0 = $v30792628861loop->length - 1; ?><?php foreach ($v30792628861iterator as $staff) { ?><?php $v30792628861loop->first = ($v30792628861incr == 0); $v30792628861loop->index = $v30792628861incr + 1; $v30792628861loop->index0 = $v30792628861incr; $v30792628861loop->revindex = $v30792628861loop->length - $v30792628861incr; $v30792628861loop->revindex0 = $v30792628861loop->length - ($v30792628861incr + 1); $v30792628861loop->last = ($v30792628861incr == ($v30792628861loop->length - 1)); ?>
        <?php if ($v30792628861loop->first) { ?>
            <table class="table">
            <tr>
                <th>Num</th>
                <th>Name</th>
                <th>Status</th>
                <th>Last Update</th>
                <?php if ($isValidToAdd && $this->length($staffs) > 1) { ?>
                    <th>Order</th>
                <?php } ?>
            </tr>
        <?php } ?>
        <tr>
            <td><?php echo $staff->room_order; ?></td>
            <td><?php echo $staff->name; ?></td>
            <td><?php echo $staff->status; ?></td>
            <td><?php echo $staff->last_update; ?></td>
            <?php if ($this->length($staffs) > 1) { ?>
            <td>
                <?php if ($isValidToAdd) { ?>
                    <?php if ($v30792628861loop->first) { ?>
                        <div class="col-xs-1">
                            <?php echo $this->tag->form(array('rooms/modify', array('method' => 'POST'))); ?>
                            <fieldset>
                                <input type="hidden" name="action" value="decrease"/>
                                <input type="hidden" name="id_of_person" value="<?php echo $staff->id_of_person; ?>"/>
                                <input type="hidden" name="id_of_room" value="<?php echo $id_of_room; ?>"/>
                                <div class="form-actions">
                                    <?php echo $this->tag->submitButton(array('DOWN', 'class' => 'btn btn-danger btn-xs')); ?>
                                </div>
                            </fieldset>
                            </form>
                        </div>
                    <?php } elseif ($v30792628861loop->last) { ?>
                        <div class="col-xs-1">
                            <?php echo $this->tag->form(array('rooms/modify', array('method' => 'POST'))); ?>
                            <fieldset>
                                <input type="hidden" name="action" value="increase"/>
                                <input type="hidden" name="id_of_person" value="<?php echo $staff->id_of_person; ?>"/>
                                <input type="hidden" name="id_of_room" value="<?php echo $id_of_room; ?>"/>
                                <div class="form-actions">
                                    <?php echo $this->tag->submitButton(array('UP', 'class' => 'btn btn-primary btn-xs')); ?>
                                </div>
                            </fieldset>
                            </form>
                        </div>
                    <?php } else { ?>
                        <div class="col-xs-1">
                            <?php echo $this->tag->form(array('rooms/modify', array('method' => 'POST'))); ?>
                            <fieldset>
                                <input type="hidden" name="action" value="increase"/>
                                <input type="hidden" name="id_of_person" value="<?php echo $staff->id_of_person; ?>"/>
                                <input type="hidden" name="id_of_room" value="<?php echo $id_of_room; ?>"/>
                                <div class="form-actions">
                                    <?php echo $this->tag->submitButton(array('UP', 'class' => 'btn btn-primary btn-xs')); ?>
                                </div>
                            </fieldset>
                            </form>
                        </div>
                        <div class="col-xs-1">
                            <?php echo $this->tag->form(array('rooms/modify', array('method' => 'POST'))); ?>
                            <fieldset>
                                <input type="hidden" name="action" value="decrease"/>
                                <input type="hidden" name="id_of_person" value="<?php echo $staff->id_of_person; ?>"/>
                                <input type="hidden" name="id_of_room" value="<?php echo $id_of_room; ?>"/>
                                <div class="form-actions">
                                    <?php echo $this->tag->submitButton(array('DOWN', 'class' => 'btn btn-danger btn-xs')); ?>
                                </div>
                            </fieldset>
                            </form>
                        </div>
                    <?php } ?>
                <?php } ?>
            </td>
            <?php } ?>
        </tr>
        <?php if ($v30792628861loop->last) { ?>
            </table>
        <?php } ?>
    <?php $v30792628861incr++; } ?>

    <?php if ($isValidToAdd) { ?>
        <div id="buttons" style="text-align: right">
            <a href="add?id_of_room=<?php echo $id_of_room; ?>&type=Staff" class="btn btn-primary smallMargin" id="addStaffButton">ADD</a>
        </div>
        <br>
    <?php } ?>
</div>

<div id="studentDiv">
    <h3>Students</h3>
    <?php $v30792628861iterator = $students; $v30792628861incr = 0; $v30792628861loop = new stdClass(); $v30792628861loop->length = count($v30792628861iterator); $v30792628861loop->index = 1; $v30792628861loop->index0 = 1; $v30792628861loop->revindex = $v30792628861loop->length; $v30792628861loop->revindex0 = $v30792628861loop->length - 1; ?><?php foreach ($v30792628861iterator as $student) { ?><?php $v30792628861loop->first = ($v30792628861incr == 0); $v30792628861loop->index = $v30792628861incr + 1; $v30792628861loop->index0 = $v30792628861incr; $v30792628861loop->revindex = $v30792628861loop->length - $v30792628861incr; $v30792628861loop->revindex0 = $v30792628861loop->length - ($v30792628861incr + 1); $v30792628861loop->last = ($v30792628861incr == ($v30792628861loop->length - 1)); ?>
        <?php if ($v30792628861loop->first) { ?>
            <table class="table">
            <tr>
                <th>Num</th>
                <th>Name</th>
                <th>Status</th>
                <th>Last Update</th>
                <?php if ($isValidToAdd && $this->length($students) > 1) { ?>
                    <th>Order</th>
                <?php } ?>
            </tr>
        <?php } ?>
        <tr>
            <td><?php echo $student->room_order; ?></td>
            <td><?php echo $student->name; ?></td>
            <td><?php echo $student->status; ?></td>
            <td><?php echo $student->last_update; ?></td>
            <?php if ($this->length($students) > 1) { ?>
                <td>
                    <?php if ($isValidToAdd) { ?>
                        <?php if ($v30792628861loop->first) { ?>
                            <div class="col-xs-1">
                                <?php echo $this->tag->form(array('rooms/modify', array('method' => 'POST'))); ?>
                                <fieldset>
                                    <input type="hidden" name="action" value="decrease"/>
                                    <input type="hidden" name="id_of_person" value="<?php echo $student->id_of_person; ?>"/>
                                    <input type="hidden" name="id_of_room" value="<?php echo $id_of_room; ?>"/>
                                    <div class="form-actions">
                                        <?php echo $this->tag->submitButton(array('DOWN', 'class' => 'btn btn-danger btn-xs')); ?>
                                    </div>
                                </fieldset>
                                </form>
                            </div>
                        <?php } elseif ($v30792628861loop->last) { ?>
                            <div class="col-xs-1">
                                <?php echo $this->tag->form(array('rooms/modify', array('method' => 'POST'))); ?>
                                <fieldset>
                                    <input type="hidden" name="action" value="increase"/>
                                    <input type="hidden" name="id_of_person" value="<?php echo $student->id_of_person; ?>"/>
                                    <input type="hidden" name="id_of_room" value="<?php echo $id_of_room; ?>"/>
                                    <div class="form-actions">
                                        <?php echo $this->tag->submitButton(array('UP', 'class' => 'btn btn-primary btn-xs')); ?>
                                    </div>
                                </fieldset>
                                </form>
                            </div>
                        <?php } else { ?>
                            <div class="col-xs-1">
                                <?php echo $this->tag->form(array('rooms/modify', array('method' => 'POST'))); ?>
                                <fieldset>
                                    <input type="hidden" name="action" value="increase"/>
                                    <input type="hidden" name="id_of_person" value="<?php echo $student->id_of_person; ?>"/>
                                    <input type="hidden" name="id_of_room" value="<?php echo $id_of_room; ?>"/>
                                    <div class="form-actions">
                                        <?php echo $this->tag->submitButton(array('UP', 'class' => 'btn btn-primary btn-xs')); ?>
                                    </div>
                                </fieldset>
                                </form>
                            </div>
                            <div class="col-xs-1">
                                <?php echo $this->tag->form(array('rooms/modify', array('method' => 'POST'))); ?>
                                <fieldset>
                                    <input type="hidden" name="action" value="decrease"/>
                                    <input type="hidden" name="id_of_person" value="<?php echo $student->id_of_person; ?>"/>
                                    <input type="hidden" name="id_of_room" value="<?php echo $id_of_room; ?>"/>
                                    <div class="form-actions">
                                        <?php echo $this->tag->submitButton(array('DOWN', 'class' => 'btn btn-danger btn-xs')); ?>
                                    </div>
                                </fieldset>
                                </form>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </td>
            <?php } ?>
        </tr>
        <?php if ($v30792628861loop->last) { ?>
            </table>
        <?php } ?>
    <?php $v30792628861incr++; } ?>

    <?php if ($isValidToAdd) { ?>
        <div id="buttons" style="text-align: right">
            <a href="add?id_of_room=<?php echo $id_of_room; ?>&type=Student" class="btn btn-primary smallMargin" id="addStaffButton">ADD</a>
        </div>
        <br>
    <?php } ?>
</div>

<?php echo $this->tag->stylesheetLink('css/register/show.css'); ?>