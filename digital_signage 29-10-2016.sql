-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 29 Okt 2016 pada 10.24
-- Versi Server: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `digital_signage`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `activity`
--

CREATE TABLE `activity` (
  `id_of_activity` int(255) NOT NULL,
  `id_of_room` varchar(10) NOT NULL,
  `id_of_person` varchar(20) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `duration` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `activity`
--

INSERT INTO `activity` (`id_of_activity`, `id_of_room`, `id_of_person`, `start_time`, `end_time`, `duration`) VALUES
(2, '51', '12/329747/TK/39033', '2016-10-02 22:50:32', '2016-10-02 23:36:00', '45 minutes'),
(3, '51', '12/329747/TK/39033', '2016-10-02 23:40:08', '2016-10-02 23:47:39', '8 minutes'),
(5, '51', '12/329747/TK/39033', '2016-10-02 23:47:59', '2016-10-02 23:48:02', '3 seconds'),
(6, '51', '12/329747/TK/39033', '2016-10-02 23:48:27', '2016-10-02 23:48:34', '7 seconds'),
(7, '51', '12/329747/TK/39033', '2016-10-03 00:08:54', '2016-10-03 00:09:43', '49 seconds'),
(8, '51', '198109212014041001', '2016-10-04 14:16:25', '2016-10-04 14:16:25', '0'),
(9, '51', '198109212014041001', '2016-10-06 14:19:15', '2016-10-06 16:27:54', '2 hours'),
(10, '51', '12/329747/TK/39033', '2016-10-06 15:15:23', '2016-10-06 15:15:23', '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `building`
--

CREATE TABLE `building` (
  `id_of_building` varchar(10) NOT NULL,
  `id_of_faculty` varchar(10) NOT NULL,
  `id_of_department` varchar(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `building`
--

INSERT INTO `building` (`id_of_building`, `id_of_faculty`, `id_of_department`, `name`, `description`, `latitude`, `longitude`) VALUES
('DTETI-MAIN', 'FT', 'DTETI', 'Main Building', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna.', -7.7660031, 110.3712655);

-- --------------------------------------------------------

--
-- Struktur dari tabel `department`
--

CREATE TABLE `department` (
  `id_of_department` varchar(10) NOT NULL,
  `id_of_faculty` varchar(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `department`
--

INSERT INTO `department` (`id_of_department`, `id_of_faculty`, `name`, `description`) VALUES
('DTETI', 'FT', 'Department of Electrical Engineering and Information Technology', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna.');

-- --------------------------------------------------------

--
-- Struktur dari tabel `faculty`
--

CREATE TABLE `faculty` (
  `id_of_faculty` varchar(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `faculty`
--

INSERT INTO `faculty` (`id_of_faculty`, `name`, `description`) VALUES
('FT', 'Faculty of Engineering', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna.');

-- --------------------------------------------------------

--
-- Struktur dari tabel `news_active`
--

CREATE TABLE `news_active` (
  `id_of_news` int(255) NOT NULL,
  `id_of_room` varchar(10) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `image_link` varchar(255) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `author` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `news_active`
--

INSERT INTO `news_active` (`id_of_news`, `id_of_room`, `title`, `content`, `image_link`, `start_time`, `end_time`, `author`) VALUES
(8, '51', 'Zemi Oktober 4', 'Mahasiswa diharapkan melaporkan progress penelitiannya pada tanggan 28 Oktober 2016 pukul 16.00 - 18.00. Kuota untuk kesempatan hari jumat adalah 6 orang. Jika lebih, maka akan dibuka kesempatan bimbingan pada hari Sabtu. Terima kasih', ' ', '2016-10-27 00:00:00', '2016-10-29 23:59:59', '12/329747/TK/39033');

-- --------------------------------------------------------

--
-- Struktur dari tabel `news_log`
--

CREATE TABLE `news_log` (
  `id_of_news` int(255) NOT NULL,
  `id_of_room` varchar(10) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `image_link` varchar(255) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `author` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `news_log`
--

INSERT INTO `news_log` (`id_of_news`, `id_of_room`, `title`, `content`, `image_link`, `start_time`, `end_time`, `author`) VALUES
(3, '51', 'Zemi Oktober II', 'Semua mahasiswa bimbingan saya harap melaporkan progress dalam zemi yang akan diadakan pada tanggal 10 Oktober 2016 pukul 16:00 di lab ini. Mohon dipersiapkan dengan baik. Terima kasih.', ' ', '2016-10-03 00:00:00', '2016-10-10 00:00:00', '198109212014041001'),
(6, '51', 'Pendaftaran Praktikum Jaringan Komputer', 'Dimohon kepada mahasiswa yang ingin mengikuti praktikum Jaringan Komputer pada semester ini harap mendaftarkan diri sebagai peserta pada tanggal 10-21 Oktober 2016. Terima kasih atas perhatiannya.', ' ', '2016-10-05 00:00:00', '2016-10-10 00:00:00', 'LECTURER00009'),
(8, '51', 'Zemi Oktober 4', 'Mahasiswa diharapkan melaporkan progress penelitiannya pada tanggan 28 Oktober 2016 pukul 16.00 - 18.00. Kuota untuk kesempatan hari jumat adalah 6 orang. Jika lebih, maka akan dibuka kesempatan bimbingan pada hari Sabtu. Terima kasih', ' ', '2016-10-27 00:00:00', '2016-10-29 23:59:59', '12/329747/TK/39033');

-- --------------------------------------------------------

--
-- Struktur dari tabel `person`
--

CREATE TABLE `person` (
  `id_of_person` varchar(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(10) NOT NULL,
  `description` varchar(255) NOT NULL,
  `image_link` varchar(255) DEFAULT NULL,
  `RFID` varchar(255) NOT NULL,
  `last_checked_in` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `person`
--

INSERT INTO `person` (`id_of_person`, `name`, `type`, `description`, `image_link`, `RFID`, `last_checked_in`) VALUES
('12/329747/TK/39033', 'Hermawan Rahmat Hidayat', 'Lecturer', 'Lecturer at Informatics and Computer Laboratory', ' ', 'tag', '2016-10-06 15:15:23'),
('194004291964031000', 'Adhi Susanto, Prof., M.Sc., Ph.D.', 'Lecturer', 'Lecturer at Electronic Systems Laboratory and Common Room', ' ', 'tag', NULL),
('194806171980031001', 'T. Haryono, Prof. Dr. Ir., M.Sc.', 'Lecturer', 'Lecturer at High Voltage Laboratory', ' ', 'tag', NULL),
('195006091980102001', 'Wahyuni R., Dr., Ir., M.Sc.', 'Lecturer', 'Lecturer at Informatics and Computer Laboratory', ' ', 'tag', NULL),
('195107071983031003', 'Mulyana, Ir.', 'Lecturer', 'Lecturer at Telecommunication and High Frequency System Laboratory', ' ', 'tag', NULL),
('195211031982032001', 'Litasari, Ir., M.Sc.', 'Lecturer', 'Lecturer at Digital System Laboratory and Doctoral Residency', ' ', 'tag', NULL),
('195312261984031001', 'Harnoko S.T., Ir., M.T.', 'Lecturer', 'Lecturer at Electrical Installation Laboratory', ' ', 'tag', NULL),
('195312271980031007', 'Sasongko Pramono Hadi, Dr., Ir., DEA', 'Lecturer', 'Lecturer at Electrical Power System Laboratory & Doctoral Residency', ' ', 'tag', NULL),
('195405281980031002', 'Samiadji Herdjunanto, Dr. Ir., M.Sc.', 'Lecturer', 'Lecturer at Control and Instrumentation Laboratory', ' ', 'tag', NULL),
('195407031988031001', 'Priyatmadi, Ir., M.T.', 'Lecturer', 'Lecturer at Control and Instrumentation Laboratory', ' ', 'tag', NULL),
('195803041988031001', 'Tiyono, Ir., M.T.', 'Lecturer', 'Lecturer at Electrical Installation Laboratory', ' ', 'tag', NULL),
('195908231986031002', 'Tumiran, Ir., M.Eng., Ph.D.', 'Lecturer', 'Lecturer at High Voltage Laboratory', ' ', 'tag', NULL),
('196003271986011001', 'Teguh Santoso, Ir.', 'Lecturer', 'Lecturer at Digital System Laboratory and Doctoral Residency', ' ', 'tag', NULL),
('196101081985031002', 'P. Insap Santosa, Ir., M.Sc., Ph.D.', 'Lecturer', 'Lecturer at Human Computer Interaction Laboratory & Common Room', ' ', 'tag', NULL),
('196104181988031001', 'Sujoko Sumaryono, Ir., M.T.', 'Lecturer', 'Lecturer at Digital System Laboratory and Doctoral Residency', ' ', 'tag', NULL),
('196109251989111001', 'Bambang Sugiyantoro, Ir., M.T.', 'Lecturer', 'Lecturer at Basic Electrical Laboratory', ' ', 'tag', NULL),
('196111091986011001', 'Wahyu Dewanto, Ir., M.T.', 'Lecturer', 'Lecturer at Basic Electronics Laboratory', ' ', 'tag', NULL),
('196210151989031001', 'Bondhan Winduratna, Dr. Ir., M.Eng.', 'Lecturer', 'Lecturer at Signal Processing Library', ' ', 'tag', NULL),
('196403151990031003', 'Rudy Hartanto, Dr. Ir., M.T.', 'Lecturer', 'Lecturer at Electronic Systems Laboratory and Common Room', ' ', 'tag', NULL),
('196404241995121001', 'Markus Nurtiantara Aji, Ir., M.T.', 'Lecturer', 'Lecturer at Informatics and Computer Laboratory', ' ', 'tag', NULL),
('196412211991031002', 'Budi Setiyanto, Ir., M.T.', 'Lecturer', 'Lecturer at Telecommunication and High Frequency System Laboratory', ' ', 'tag', NULL),
('196510041993031003', 'M. Isnaeni B.S., Ir., M.T.', 'Lecturer', 'Lecturer at Transmission and Distribution Laboratory', ' ', 'tag', NULL),
('196603271991031002', 'Lukito Edi Nugroho, Ir., M.Sc., Ph.D.', 'Lecturer', 'Lecturer at Digital System Laboratory and Doctoral Residency', ' ', 'tag', NULL),
('196708021993031002', 'Risanuri Hidayat, Dr., Ir., M.Sc.', 'Lecturer', 'Lecturer at Digital System Laboratory and Doctoral Residency', ' ', 'tag', NULL),
('196711281994121001', 'Oyas Wahyunggoro, Ir., M.T., Ph.D.', 'Lecturer', 'Lecturer at Control and Instrumentation Laboratory', ' ', 'tag', NULL),
('196907262000121001', 'Dani Adhipta, S.Si., M.T.', 'Lecturer', 'Lecturer at Informatics and Computer Laboratory', ' ', 'tag', NULL),
('196909201995121001', 'Teguh Bharata Adji, S.T., M.T., M.Eng., Ph.D.', 'Lecturer', 'Lecturer at Informatics and Computer Laboratory', ' ', 'tag', NULL),
('196911221995122001', 'Sri Suning Kusumawardani, Dr., S.T., M.T.', 'Lecturer', 'Lecturer at Cisco Networking Academy and Microsoft Innovation Center', ' ', 'tag', NULL),
('197111011997022001', 'Silmi Fauziati, Dr.Eng., S.T., M.T.', 'Lecturer', 'Lecturer at Informatics and Computer Laboratory', ' ', 'tag', NULL),
('197111171998031003', 'Addin Suwastono, S.T., M.Eng.', 'Lecturer', 'Lecturer at Digital System Laboratory and Doctoral Residency', ' ', 'tag', NULL),
('197203011997021001', 'Selo, S.T., M.T., M.Sc., Ph.D.', 'Lecturer', 'Lecturer at Electronic Systems Laboratory and Common Room', ' ', 'tag', NULL),
('197307061999031005', 'Sarjiya, S.T., M.T., Ph.D.', 'Lecturer', 'Lecturer at Electrical Power System Laboratory & Doctoral Residency', ' ', 'tag', NULL),
('197311251998031003', 'Warsun Najib, S.T., M.Sc.', 'Lecturer', 'Lecturer at Computer Network and Distributed Applications Laboratory & Doctoral Residency', ' ', 'tag', NULL),
('197312042002121001', 'Widyawan, S.T., M.Sc., Ph.D.', 'Lecturer', 'Lecturer at Computer Network and Distributed Applications Laboratory & Doctoral Residency', ' ', 'tag', NULL),
('197402261998031003', 'Dr. Eng. Danang Wijaya S.T., M.T.', 'Lecturer', 'Lecturer at Electrical Power System Laboratory & Doctoral Residency', ' ', 'tag', NULL),
('197406152005012001', 'Indah Soesanti, Dr., S.T., M.T.', 'Lecturer', 'Lecturer at Signal Processing Library', ' ', 'tag', NULL),
('197501152005011000', 'Prapto Nugroho, S.T., M.Eng., D.Eng.', 'Lecturer', 'Lecturer at Basic Electronics Laboratory', ' ', 'tag', NULL),
('197506071999031000', 'Noor Akhmad Setiawan, S.T., M.T., Ph.D.', 'Lecturer', 'Lecturer at Informatics and Computer Laboratory', ' ', 'tag', NULL),
('197506192002121004', 'M. Nur Rizal, S.T., M.Eng., Ph.D.', 'Lecturer', 'Lecturer at Informatics and Computer Laboratory', ' ', 'tag', NULL),
('197510082002121003', 'Harry Prabowo, S.T., M.T.', 'Lecturer', 'Lecturer at High Voltage Laboratory', ' ', 'tag', NULL),
('197604152002121000', 'Iswandi, S.T., M.Eng.', 'Lecturer', 'Lecturer at Basic Electrical Laboratory', ' ', 'tag', NULL),
('197605012002121000', 'Sigit Basuki Wibowo, S.T., M.Eng.', 'Lecturer', 'Lecturer at Telecommunication and High Frequency System Laboratory', ' ', 'tag', NULL),
('197611121999031002', 'Suharyanto, Dr.Eng., S.T., M.Eng.', 'Lecturer', 'Lecturer at High Voltage Laboratory', ' ', 'tag', NULL),
('197701312002121003', 'Bimo Sunarfri Hantono, S.T., M.Eng.', 'Lecturer', 'Lecturer at Electronic Systems Laboratory and Common Room', ' ', 'tag', NULL),
('197802242002121001', 'Hanung Adi Nugroho, S.T., M.E., Ph.D.', 'Lecturer', 'Lecturer at Signal Processing Library', ' ', 'tag', NULL),
('197903032002121004', 'Eka Firmansyah, S.T., M.Eng., Ph.D.', 'Lecturer', 'Lecturer at Electrical Power System Laboratory & Doctoral Residency', ' ', 'tag', NULL),
('197905262002122001', 'Indriana Hidayah, S.T., M.T.', 'Lecturer', 'Lecturer at Informatics and Computer Laboratory', ' ', 'tag', NULL),
('197911022008121000', 'Adha Imam Cahyadi, S.T., M.Eng., D.Eng.', 'Lecturer', 'Lecturer at Control and Instrumentation Laboratory', ' ', 'tag', NULL),
('198004032005012003', 'Avrin Nur Widiastuti, S.T., M.Eng.', 'Lecturer', 'Lecturer at Basic Electrical Laboratory', ' ', 'tag', NULL),
('198103262008122002', 'Eny Sukani Rahayu, S.T., M.Eng.', 'Lecturer', 'Lecturer at Basic Electrical Laboratory', ' ', 'tag', NULL),
('198104292008122001', 'Adhistya Erna Permanasari, S.T., M.T., Ph.D.', 'Lecturer', 'Lecturer at Informatics and Computer Laboratory', ' ', 'tag', NULL),
('198109212014041001', 'I Wayan Mustika, S.T., M.Eng., Ph.D.', 'Lecturer', 'Lecturer at Electronic Systems Laboratory and Common Room', ' ', 'tag', '2016-10-06 14:19:15'),
('198310202008121002', 'Ridi Ferdiana, Dr., S.T., M.T.', 'Lecturer', 'Lecturer at Cisco Networking Academy and Microsoft Innovation Center', ' ', 'tag', NULL),
('198501182012121003', 'Fikri Waskito, S.T., M.Eng.', 'Lecturer', 'Lecturer at Transmission and Distribution Laboratory', ' ', 'tag', NULL),
('198510262015041003', 'Sunu Wibirama , Dr.Eng., S.T., M.Eng.', 'Lecturer', 'Lecturer at Signal Processing Library', ' ', 'tag', NULL),
('LECTURER00000', 'Lesnanto Multa Putranto, S.T., M.Eng.', 'Lecturer', 'Lecturer at Electrical Power System Laboratory & Doctoral Residency', ' ', 'tag', NULL),
('LECTURER00001', 'Yusuf Susilo Wijoyo, S.T., M.Eng.', 'Lecturer', 'Lecturer at Electrical Installation Laboratory', ' ', 'tag', NULL),
('LECTURER00002', 'Husni Rois Ali, S.T., M.Eng.', 'Lecturer', 'Lecturer at Transmission and Distribution Laboratory', ' ', 'tag', NULL),
('LECTURER00003', 'Azkario Rizky Pratama, S.T., M.Eng.', 'Lecturer', 'Lecturer at Informatics and Computer Laboratory', ' ', 'tag', NULL),
('LECTURER00004', 'Ahmad Nasikun, S.T., M.Sc.', 'Lecturer', 'Lecturer at Informatics and Computer Laboratory', ' ', 'tag', NULL),
('LECTURER00005', 'Agus Bejo, S.T., M.Eng, D.Eng.', 'Lecturer', 'Lecturer at Digital System Laboratory and Doctoral Residency', ' ', 'tag', NULL),
('LECTURER00006', 'Igi Ardiyanto, Dr.Eng., S.T., M.Eng.', 'Lecturer', 'Lecturer at Signal Processing Laboratory', ' ', 'tag', NULL),
('LECTURER00007', 'Dyonisius Dony Ariananda S.T., M.Sc., Ph.D.', 'Lecturer', 'Lecturer at Telecommunication and High Frequency System Laboratory', ' ', 'tag', NULL),
('LECTURER00008', 'Anugerah Galang Persada, S.T., M.Eng.', 'Lecturer', 'Lecturer at Telecommunication and High Frequency System Laboratory', ' ', 'tag', NULL),
('LECTURER00009', 'Heruwanto, S.T., M.M.', 'Staff', 'Staff at Electronic Systems Laboratory and Common Room', ' ', 'tag', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `room`
--

CREATE TABLE `room` (
  `id_of_room` int(255) NOT NULL,
  `id_of_building` varchar(11) DEFAULT NULL,
  `code_room` varchar(10) NOT NULL,
  `floor` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text,
  `image_link` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `room`
--

INSERT INTO `room` (`id_of_room`, `id_of_building`, `code_room`, `floor`, `name`, `description`, `image_link`) VALUES
(1, 'DTETI-MAIN', 'N101', 1, 'Finance', NULL, NULL),
(2, 'DTETI-MAIN', 'N102', 1, 'Common Room & Meeting Room', NULL, NULL),
(3, 'DTETI-MAIN', 'N103', 1, 'Common Room', NULL, NULL),
(4, 'DTETI-MAIN', 'N104', 1, 'Electrical Power System Laboratory', NULL, NULL),
(5, 'DTETI-MAIN', 'N105', 1, 'Toilet', NULL, NULL),
(6, 'DTETI-MAIN', 'N106', 1, 'Electrical Installation Laboratory', NULL, NULL),
(7, 'DTETI-MAIN', 'N107', 1, 'Electrical Power System Laboratory & Doctoral Residency', NULL, NULL),
(8, 'DTETI-MAIN', 'N108', 1, 'Alumni', NULL, NULL),
(9, 'DTETI-MAIN', 'N109', 1, 'Finance', NULL, NULL),
(10, 'DTETI-MAIN', 'S101', 1, 'Academic Affairs & Head of Department', NULL, NULL),
(11, 'DTETI-MAIN', 'S102', 1, 'Meeting Room', NULL, NULL),
(12, 'DTETI-MAIN', 'S103', 1, 'Board of Directors', NULL, NULL),
(13, 'DTETI-MAIN', 'S104', 1, 'Teleconference Room', NULL, NULL),
(14, 'DTETI-MAIN', 'S105', 1, 'High Voltage Laboratory', NULL, NULL),
(15, 'DTETI-MAIN', 'S106', 1, 'Depo', NULL, NULL),
(16, 'DTETI-MAIN', 'S107', 1, 'Class Room', NULL, NULL),
(17, 'DTETI-MAIN', 'S108', 1, 'Transmission and Distribution Laboratory', NULL, NULL),
(18, 'DTETI-MAIN', 'S109', 1, 'Toilet', NULL, NULL),
(19, 'DTETI-MAIN', 'S110', 1, 'Library', NULL, NULL),
(20, 'DTETI-MAIN', 'S111', 1, 'Lecturers Transit Room', NULL, NULL),
(21, 'DTETI-MAIN', 'N201', 2, 'Class Room', NULL, NULL),
(22, 'DTETI-MAIN', 'N202', 2, 'Common Room', NULL, NULL),
(23, 'DTETI-MAIN', 'N203', 2, 'Class Room', NULL, NULL),
(24, 'DTETI-MAIN', 'N204', 2, 'Toilet', NULL, NULL),
(25, 'DTETI-MAIN', 'N205', 2, 'Common Room', NULL, NULL),
(26, 'DTETI-MAIN', 'N206', 2, 'Basic Electrical Laboratory', NULL, NULL),
(27, 'DTETI-MAIN', 'N207', 2, 'Basic Electronics Laboratory', NULL, NULL),
(28, 'DTETI-MAIN', 'S201', 2, 'Student and Internal Affairs', NULL, NULL),
(29, 'DTETI-MAIN', 'S202', 2, 'IT, Network, and Servers', NULL, NULL),
(30, 'DTETI-MAIN', 'S203', 2, 'Schneider Electric Training Center', NULL, NULL),
(31, 'DTETI-MAIN', 'S204', 2, 'High Voltage Laboratory', NULL, NULL),
(32, 'DTETI-MAIN', 'S205', 2, 'Class Room', NULL, NULL),
(33, 'DTETI-MAIN', 'S206', 2, 'Informatics and Computer Laboratory', NULL, NULL),
(34, 'DTETI-MAIN', 'S207', 2, 'Cisco Networking Academy and Microsoft Innovation Center', NULL, NULL),
(35, 'DTETI-MAIN', 'S208', 2, 'Human Computer Interaction Laboratory & Common Room', NULL, NULL),
(36, 'DTETI-MAIN', 'S209', 2, 'Toilet', NULL, NULL),
(37, 'DTETI-MAIN', 'S210', 2, 'Class Room', NULL, NULL),
(38, 'DTETI-MAIN', 'S211', 2, 'Class Room', NULL, NULL),
(39, 'DTETI-MAIN', 'N301', 3, 'Telecommunication and High Frequency System Laboratory', NULL, NULL),
(40, 'DTETI-MAIN', 'N302', 3, 'Toilet', NULL, NULL),
(41, 'DTETI-MAIN', 'N303', 3, 'Depo', NULL, NULL),
(42, 'DTETI-MAIN', 'N304', 3, 'Signal Processing Library', NULL, NULL),
(43, 'DTETI-MAIN', 'N305', 3, 'Control and Instrumentation Laboratory', NULL, NULL),
(44, 'DTETI-MAIN', 'N306', 3, 'Meeting Room', NULL, NULL),
(45, 'DTETI-MAIN', 'N307', 3, 'Depo', NULL, NULL),
(46, 'DTETI-MAIN', 'N308', 3, 'Class Room', NULL, NULL),
(47, 'DTETI-MAIN', 'S301', 3, 'Digital System Laboratory and Doctoral Residency', NULL, NULL),
(48, 'DTETI-MAIN', 'S302', 3, 'Computer Network and Distributed Applications Laboratory & Doctoral Residency', NULL, NULL),
(49, 'DTETI-MAIN', 'S303', 3, 'Class Room', NULL, NULL),
(50, 'DTETI-MAIN', 'S304', 3, 'Class Room', NULL, NULL),
(51, 'DTETI-MAIN', 'S305', 3, 'Electronic Systems Laboratory and Common Room', NULL, NULL),
(52, 'DTETI-MAIN', 'S306', 3, 'Toilet', NULL, NULL),
(53, 'DTETI-MAIN', 'S307', 3, 'Class Room', NULL, NULL),
(54, 'DTETI-MAIN', 'S308', 3, 'Class Room', NULL, NULL),
(55, 'DTETI-MAIN', 'S309', 3, 'Class Room', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `staff`
--

CREATE TABLE `staff` (
  `id_of_staff` int(255) NOT NULL,
  `id_of_person` varchar(20) NOT NULL,
  `id_of_room` varchar(10) NOT NULL,
  `email` varchar(255) NOT NULL,
  `encrypted_password` varchar(255) NOT NULL,
  `salt` varchar(100) NOT NULL,
  `is_admin` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `staff`
--

INSERT INTO `staff` (`id_of_staff`, `id_of_person`, `id_of_room`, `email`, `encrypted_password`, `salt`, `is_admin`) VALUES
(5, '12/329747/TK/39033', '51', 'hermawan.hidayat@te.gadjahmada.edu', 'XpEpFWpoVCEQUxNO0msl6G0cDSQ0NmE3Y2ZlNTUw', '46a7cfe550', 1),
(6, '197402261998031003', '7', 'danangwijaya@ugm.ac.id', 'EYMLu9wDLZM9JHqBbuamqdoJbSgwNzNjYzdhZWI5', '073cc7aeb9', 1),
(7, '195312271980031007', '7', 'sasongko@ugm.ac.id', '6tLz1qIqKQBLbrOOlqtZh/R2l+JjMmVjODQ4YTI4', 'c2ec848a28', 1),
(8, '197903032002121004', '7', 'eka_firmanyah@ugm.ac.id', 'xL8CduZt+O8lzln2c2JoTYSgg9VmOTQ3YzVmNjVh', 'f947c5f65a', 1),
(9, '197307061999031005', '7', 'sarjiya@ugm.ac.id', 'MUMCkvN/FXcbqRkxgKlHWvN8tkM4NDA5YzBjOTEz', '8409c0c913', 1),
(10, 'LECTURER00000', '7', 'lesnanto@ugm.ac.id', 'Tm8/APAxymVb/2HSMNEcGFsloQE5ODliNjZmNDUw', '989b66f450', 1),
(11, 'LECTURER00001', '6', 'yusufsw@ugm.ac.id', 'b7EiWifvxIkRSX/FgxZyyoZm/jUxM2MwNjEzZTY3', '13c0613e67', 1),
(12, '195803041988031001', '6', 'tyn@ugm.ac.id', 'OCWkkXpPZQpK70I0+s3l1KHVrx4yODg3ZDBlZGE0', '2887d0eda4', 1),
(13, '195312261984031001', '6', 'harnoko@ugm.ac.id', 'u+w25w2dhu8g+41Epk6MA/UfgXQ1M2VmYjFkNmIw', '53efb1d6b0', 1),
(14, '197611121999031002', '14', 'suharyanto@te.ugm.ac.id', 'bjsQxQHyc7qK6G4Hsf8Mo2T0Ujk2YjFhMjlmNzVj', '6b1a29f75c', 1),
(15, '194806171980031001', '14', 'thr@ugm.ac.id', 'FeYGV0D6DFRCnPX6Bp5ay/+F/7swMWZmOTZiNGZi', '01ff96b4fb', 1),
(16, '195908231986031002', '14', 'tumiran@ugm.ac.id', 'Mru0nadz4qh/FEp/qnOkXuR24ntiNmU3Y2FhZmNk', 'b6e7caafcd', 1),
(17, '197510082002121003', '14', 'harryprabowo@ugm.ac.id', '/SfoZKO6/Y+MZYs3Vu3ALQSLu+gwMDIyNjA4OGMw', '00226088c0', 1),
(18, '196510041993031003', '17', 'm.isnaeni@ugm.ac.id', 'tOLTi/ZlJS1GhcnBBbxetETJnspmMTFjYTgwZDQ2', 'f11ca80d46', 1),
(19, 'LECTURER00002', '17', 'husni.rois.ali@ugm.ac.id', 'I7UZI4c6CtxlFYVHZE2qLGQ1HE1iNGY5NDBhY2Y3', 'b4f940acf7', 1),
(20, '198501182012121003', '17', 'fikri@ugm.ac.id', 'ciRCIoRXBa5B8AW0VpdRROcLa0E0MGIwNmIzMDRl', '40b06b304e', 1),
(21, '196109251989111001', '26', 'bsg@ugm.ac.id', 'cOSFJu1oXxYEiSRWMcNd17ZmlytlZmZjZDE5Zjdj', 'effcd19f7c', 1),
(22, '198004032005012003', '26', 'avrin@ugm.ac.id', '+KxhFQiJZf/oYB5coL5jjGLugLNlNDc0ZDJkZjVi', 'e474d2df5b', 1),
(23, '198103262008122002', '26', 'eny_sr@ugm.ac.id', '+0ecLCIfYTOnRMSPtuYSIKebbTUwZjhhZmIxMzFh', '0f8afb131a', 1),
(24, '197604152002121000', '26', 'iswandi@ugm.ac.id', 'pu48SANkoYhDtf2NkOk0uBxse3BmODk4ODFkOGE3', 'f89881d8a7', 1),
(25, '197501152005011000', '27', 'tatok@ugm.ac.id', 'L6kd+iU59y+HgMscKQ13S0/zauhiZmM1YmI3MTNk', 'bfc5bb713d', 1),
(26, '196111091986011001', '27', 'wahyud@ugm.ac.id', 'eHiTN6j/IHLOBRb6xHv/k1w19RBjN2RjMmE2Nzc5', 'c7dc2a6779', 1),
(27, '197111011997022001', '33', 'silmi@ugm.ac.id', 'RiS94xNE97v4dJzLXO53Nmn6muc0MjFkZmM2NGVl', '421dfc64ee', 1),
(28, '195006091980102001', '33', 'ibu_yuni@ugm.ac.id', 'xWOd+ZKVX1d2/yZEFflNMiSgnhA1OWMxYmQxMzY2', '59c1bd1366', 1),
(29, '196404241995121001', '33', 'mna@ugm.ac.id', 'vO2QqzisCbq3ULcl7rIr6JJl82U5YjY1NmMyNDM1', '9b656c2435', 1),
(30, '196909201995121001', '33', 'adji@ugm.ac.id', 'Zj2jdR/Cp+o1zxhmNE242u0xAbQ1OWI5M2Y5NDIx', '59b93f9421', 1),
(31, '197905262002122001', '33', 'indriana.h@ugm.ac.id', '636Tlivj12xDNG+5LHFoZXKALUxhM2FiZmIzMjAy', 'a3abfb3202', 1),
(32, '198104292008122001', '33', 'adhistya@ugm.ac.id', 'S3AEmN/QskFUFSJYjPjtsPisT59kNmVmNWU4Njcx', 'd6ef5e8671', 1),
(33, '196907262000121001', '33', 'dani@ugm.ac.id', 'AqA3qJObtRQ3o+ElrzrNTXQfxaIwNjNiY2FmMWU3', '063bcaf1e7', 1),
(34, '197506071999031000', '33', 'noorwewe@ugm.ac.id', 'zYTnwRV2xr6PmwGPqKLDqRt4bCVlZTMyYmQ5N2U3', 'ee32bd97e7', 1),
(35, 'LECTURER00003', '33', 'azkario@ugm.ac.id', 'Mea+pGbAZS9QGZ86c5bCDzwcUXZjMjdkMWQ3YTU0', 'c27d1d7a54', 1),
(36, '197506192002121004', '33', 'mnrizal@ugm.ac.id', '5woFiTe/Eor6aNBM+09MDttwH99hZDM5M2ExOGRj', 'ad393a18dc', 1),
(37, 'LECTURER00004', '33', 'ahmad.nasikun@ugm.ac.id', 'f5GdWCkLhAFGiSKxWGG5Lq4Hj9s0YWZlOTg0NTNj', '4afe98453c', 1),
(38, '196911221995122001', '34', 'suning@ugm.ac.id', '8FpXAjG9Pvm9eVy9b8/1uMriKhk3NTZhY2E1MmQ1', '756aca52d5', 1),
(39, '198310202008121002', '34', 'ridi@ugm.ac.id', 'CE8n2It9/aFVihDjWhbchias5tQzYWE1YzJjYWVj', '3aa5c2caec', 1),
(40, '196101081985031002', '35', 'insap@ugm.ac.id', '8+GgGaUdCTCbqesLnHeQwqQ/+f45NTdiY2ViMzAx', '957bceb301', 1),
(41, '197203011997021001', '51', 'selo@ugm.ac.id', 'mYyCSeMxA1FrmQmG5i+JcbNyfKs2MDEwYmM2ZDFi', '6010bc6d1b', 1),
(42, '194004291964031000', '51', 'adhisusanto@ugm.ac.id', '8W4dzS+IYKx/HpCDwJ5GYJkN08c2YWY0YTVmYjc2', '6af4a5fb76', 1),
(43, '196403151990031003', '51', 'rudy@ugm.ac.id', 'YX3U/n4uVgrWqlfEWxdZuz7S3lk4YjhjOWM1OThh', '8b8c9c598a', 1),
(44, '197701312002121003', '51', 'bhe@ugm.ac.id', 'NC33FFJJNq81pe0T37nSnfow7/Y5ZjMwZmYzNTlk', '9f30ff359d', 1),
(45, '198109212014041001', '51', 'wmustika@ugm.ac.id', 'J4l0oXS8S9uu9DuQQvnWLZKisTY1ZGY5NzY5ZmY2', '5df9769ff6', 1),
(46, '196708021993031002', '47', 'risanuri@ugm.ac.id', 'YfhpKeIFdlxLWwggSzyVXWfkSfkzZDc3ZjljMWI1', '3d77f9c1b5', 1),
(47, '195211031982032001', '47', 'litasari@ugm.ac.id', 'j99mLjEp7Jjm2T9oimYak2YvhEhjNDhjNjhmODJj', 'c48c68f82c', 1),
(48, '196104181988031001', '47', 'sujoko@ugm.ac.id', 'GrhEFsGQB60NQE8ccxWsRGcs+ys4ZTk1YzBlZjRm', '8e95c0ef4f', 1),
(49, '197111171998031003', '47', 'addins@ugm.ac.id', 'meWT70z4SMEl1mLdjH/kk4vXLyI1ZDQzZTM2ZjZl', '5d43e36f6e', 1),
(50, '196003271986011001', '47', 'tsanto-so@ugm.ac.id', 'ZGS+O4gFAyxIoxmxbPHWgLMjzbxlOGI5YWQyYTE0', 'e8b9ad2a14', 1),
(51, 'LECTURER00005', '47', 'agusbj@ugm.ac.id', '3WSh7LEFVE1TWaWWB8KlBpR7sTUzNWZiMDdiNDc4', '35fb07b478', 1),
(52, 'LECTURER00006', '42', 'igi@ugm.ac.id', 'l/+VTBU9SAeJ9j4ltkW7aHBcP7VlODExNjI5ZjVk', 'e811629f5d', 1),
(53, '197311251998031003', '48', 'warsun@ugm.ac.id', '2e/tfwpmpXf3zHQQnS+Z2nLF/LMwN2ZhMjI0ODE2', '07fa224816', 1),
(54, '196603271991031002', '48', 'lukito@ugm.ac.id', 'Opff5Z9idAPHO+pWUG7D70mz7EAyNTA4ODcwNmQx', '25088706d1', 1),
(55, '197312042002121001', '48', 'widyawan@ugm.ac.id', 'cChlGfdW22jzEkwXemxvMRzh0ABlZWU0YjFlM2Mx', 'eee4b1e3c1', 1),
(56, '196711281994121001', '43', 'oyas@ugm.ac.id', '4oeAq+KBj7b1pTRCyyv+hQYo2ahmNDk0MTY5NWZl', 'f4941695fe', 1),
(57, '195405281980031002', '43', 'samiadji@ugm.ac.id', 'R5jrAImFDw24IVo+dHOp8wXUSiYwMjgzMzkyYThk', '0283392a8d', 1),
(58, '195407031988031001', '43', 'priyatmadi@ugm.ac.id', '5obFZeZIoNbDElDzMb5VVJk9jvBhNGY4ODgxYmEz', 'a4f8881ba3', 1),
(59, '197911022008121000', '43', 'adha.imam@ugm.ac.id', 'icVmIhtBcTsD9DfGQcJbdB2xqng0YTE2MDRhNTVk', '4a1604a55d', 1),
(60, '197406152005012001', '42', 'indahsoesanti@ugm.ac.id', 'ah42JYyOIsjpT2K6SG/UqA9C3TswNjkwMmE2Y2Y5', '06902a6cf9', 1),
(61, '196210151989031001', '42', 'windurat@ugm.ac.id', '2rK1AQzQIid5R6pMrSRkDAdQkshjN2QzOGE5YTNl', 'c7d38a9a3e', 1),
(62, '197802242002121001', '42', 'adnugroho@ugm.ac.id', 'XzZMpLJBQuwdF8WnbgluEMHmDI43MTI1Yjc1YzA1', '7125b75c05', 1),
(63, '198510262015041003', '42', 'sunu@ugm.ac.id', 'iNsz0kAg9bnALMyy+eT9sPI64PU4MDBlY2RhMjRh', '800ecda24a', 1),
(64, '197605012002121000', '39', 'sigitbw@ugm.ac.id', 'kvEpVYCL1WDoP5oSPns90bszx0hkZDcwZDVjMGZh', 'dd70d5c0fa', 1),
(65, '196412211991031002', '39', 'budi_setiyanto@ugm.ac.id', '6qyhEFLgngP2YDvnAZiUUArOu5E3Nzg3MzY3NGU1', '77873674e5', 1),
(66, '195107071983031003', '39', 'mulyana.15@ugm.ac.id', 'i6DYzddFQ8AKq8XZyxppSDoC30JiMGIyM2Q2YzA2', 'b0b23d6c06', 1),
(67, 'LECTURER00007', '39', 'dyonisius.doni@ugm.ac.id', 'XjkJP5kZ8foioOvOa+qE6RWKWc05NjA4NjczMmZk', '96086732fd', 1),
(68, 'LECTURER00008', '39', 'galang@ugm.ac.id', 'uzjvMlgWFRHEL3G8cUG+uTchdI5hNmQ2NmY2M2Vl', 'a6d66f63ee', 1),
(69, 'LECTURER00009', '51', 'heruwanto@ugm.ac.id', 'Se/W+QSoDy+VNeil+71dStZx7R1kZDQ3OWQ3NjRk', 'dd479d764d', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `today_activity`
--

CREATE TABLE `today_activity` (
  `id_of_activity` int(255) NOT NULL,
  `id_of_room` varchar(10) NOT NULL,
  `id_of_person` varchar(20) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `duration` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `today_activity`
--

INSERT INTO `today_activity` (`id_of_activity`, `id_of_room`, `id_of_person`, `start_time`, `end_time`, `duration`) VALUES
(2, '51', '12/329747/TK/39033', '2016-10-02 22:50:32', '2016-10-02 23:36:00', '45 minutes'),
(3, '51', '12/329747/TK/39033', '2016-10-02 23:40:08', '2016-10-02 23:47:39', '8 minutes'),
(5, '51', '12/329747/TK/39033', '2016-10-02 23:47:59', '2016-10-02 23:48:02', '3 seconds'),
(6, '51', '12/329747/TK/39033', '2016-10-02 23:48:27', '2016-10-02 23:48:34', '7 seconds'),
(7, '51', '12/329747/TK/39033', '2016-10-03 00:08:54', '2016-10-03 00:09:43', '49 seconds');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity`
--
ALTER TABLE `activity`
  ADD PRIMARY KEY (`id_of_activity`);

--
-- Indexes for table `building`
--
ALTER TABLE `building`
  ADD PRIMARY KEY (`id_of_building`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id_of_department`);

--
-- Indexes for table `faculty`
--
ALTER TABLE `faculty`
  ADD PRIMARY KEY (`id_of_faculty`);

--
-- Indexes for table `news_active`
--
ALTER TABLE `news_active`
  ADD PRIMARY KEY (`id_of_news`);

--
-- Indexes for table `news_log`
--
ALTER TABLE `news_log`
  ADD PRIMARY KEY (`id_of_news`);

--
-- Indexes for table `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`id_of_person`);

--
-- Indexes for table `room`
--
ALTER TABLE `room`
  ADD PRIMARY KEY (`id_of_room`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`id_of_staff`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `today_activity`
--
ALTER TABLE `today_activity`
  ADD PRIMARY KEY (`id_of_activity`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity`
--
ALTER TABLE `activity`
  MODIFY `id_of_activity` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `news_log`
--
ALTER TABLE `news_log`
  MODIFY `id_of_news` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `room`
--
ALTER TABLE `room`
  MODIFY `id_of_room` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `id_of_staff` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;
--
-- AUTO_INCREMENT for table `today_activity`
--
ALTER TABLE `today_activity`
  MODIFY `id_of_activity` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
