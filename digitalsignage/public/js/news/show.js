/**
 * Created by HermawanRahmatHidaya on 03/10/2016.
 */

jQuery(document).ready(function($) {
    Date.prototype.toDateInputValue = (function() {
        var local = new Date(this);
        local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
        return local.toJSON().slice(0,10);
    });

    $(".btn-view").click(function() {
        var row = $(this).closest("tr");
        var author = row.find("td:nth-child(1)");
        var title = row.find("td:nth-child(2)");
        var content = row.data("content");
        var startTime = row.find("td:nth-child(3)");
        var endTime = row.find("td:nth-child(4)");
        $("#viewModal").modal('show');

        $("#view-modal-title").html(title.text());
        $("#view-content").html(content);
        $("#view-author").html("Written by : "+author.text());
        $("#view-start-time").html("Start Time : "+startTime.text());
        $("#view-end-time").html("End Time : "+endTime.text());
    });

    $(".btn-edit").click(function() {
        var row = $(this).closest("tr");
        $('#formEdit').prop("action","edit");
        var author = row.data("id-of-person");
        var title = row.find("td:nth-child(2)");
        var content = row.data("content");
        var startTime = row.find("td:nth-child(3)");
        var endTime = row.find("td:nth-child(4)");
        var idOfNews = row.data("id-of-news");
        $("#editModal").modal('show');
        $("#edit-modal-title").html('Edit a News');

        $("#title").val(title.text());
        $("#title").prop('readonly', false);
        $("#content").val(content);
        $("#content").prop('readonly', false);
        $("#startTime").val(new Date(startTime.text()).toDateInputValue());
        $("#startTime").prop('readonly', false);
        $("#endTime").val(new Date(endTime.text()).toDateInputValue());
        $("#endTime").prop('readonly', false);
        $("#idOfNews").val(idOfNews);
        $("#author").val(author);

        $("#editButton").val("SAVE");
        $("#editButton").prop("class", "btn btn-warning");
    });

    $(".btn-delete").click(function() {
        var row = $(this).closest("tr");
        $('#formEdit').prop("action","delete");
        var author = row.data("id-of-person");
        var title = row.find("td:nth-child(2)");
        var content = row.data("content");
        var startTime = row.find("td:nth-child(3)");
        var endTime = row.find("td:nth-child(4)");
        var idOfNews = row.data("id-of-news");
        $("#editModal").modal('show');
        $("#edit-modal-title").html('Delete a News');

        $("#title").val(title.text());
        $("#title").prop('readonly', true);
        $("#content").val(content);
        $("#content").prop('readonly', true);
        $("#startTime").val(new Date(startTime.text()).toDateInputValue());
        $("#startTime").prop('readonly', true);
        $("#endTime").val(new Date(endTime.text()).toDateInputValue());
        $("#endTime").prop('readonly', true);
        $("#idOfNews").val(idOfNews);


        $("#editButton").val("DELETE");
        $("#editButton").prop("class", "btn btn-danger");
    });
});

function formatDateString(s) {
    var s = s.split(/\D/);
    var date = new Date(s);
    return s[2] + '-' + s[1] + '-' + s[0];
}

// var datepickers = $(".datepicker").datepicker({ dateFormat: "yy-mm-dd" }).val();

$.each($(".datepicker"), function (i, val) {
    // val.datepicker({ dateFormat: "yy-mm-dd" }).val();
    console.log(val);
});
