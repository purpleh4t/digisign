<?php echo $this->getContent(); ?>

<?php echo $this->headerElements->getTabs(); ?>

<h1><?php echo $title; ?></h1>

<div id="lecturerDiv">
    <h3>Lecturers</h3>
    <?php $v15690813561iterator = $lecturers; $v15690813561incr = 0; $v15690813561loop = new stdClass(); $v15690813561loop->length = count($v15690813561iterator); $v15690813561loop->index = 1; $v15690813561loop->index0 = 1; $v15690813561loop->revindex = $v15690813561loop->length; $v15690813561loop->revindex0 = $v15690813561loop->length - 1; ?><?php foreach ($v15690813561iterator as $lecturer) { ?><?php $v15690813561loop->first = ($v15690813561incr == 0); $v15690813561loop->index = $v15690813561incr + 1; $v15690813561loop->index0 = $v15690813561incr; $v15690813561loop->revindex = $v15690813561loop->length - $v15690813561incr; $v15690813561loop->revindex0 = $v15690813561loop->length - ($v15690813561incr + 1); $v15690813561loop->last = ($v15690813561incr == ($v15690813561loop->length - 1)); ?>
        <?php if ($v15690813561loop->first) { ?>
            <table class="table">
            <tr>
                <th>Num</th>
                <th>Name</th>
                <th>Status</th>
                <th>Last Update</th>
                <?php if ($isValidToAdd && $this->length($lecturers) > 1) { ?>
                    <th>Order</th>
                <?php } ?>
            </tr>
        <?php } ?>
        <tr>
            <td><?php echo $lecturer->room_order; ?></td>
            <td><?php echo $lecturer->name; ?></td>
            <td><?php echo $lecturer->status; ?></td>
            <td><?php echo $lecturer->last_update; ?></td>
            <?php if ($this->length($lecturers) > 1) { ?>
            <td>
                <?php if ($isValidToAdd) { ?>
                    <?php if ($v15690813561loop->first) { ?>
                        <div class="col-xs-1">
                            <?php echo $this->tag->form(array('rooms/modify', array('method' => 'POST'))); ?>
                            <fieldset>
                                <input type="hidden" name="action" value="decrease"/>
                                <input type="hidden" name="id_of_person" value="<?php echo $lecturer->id_of_person; ?>"/>
                                <input type="hidden" name="id_of_room" value="<?php echo $id_of_room; ?>"/>
                                <div class="form-actions">
                                    <?php echo $this->tag->submitButton(array('DOWN', 'class' => 'btn btn-danger btn-xs')); ?>
                                </div>
                            </fieldset>
                            </form>
                        </div>
                    <?php } elseif ($v15690813561loop->last) { ?>
                        <div class="col-xs-1">
                            <?php echo $this->tag->form(array('rooms/modify', array('method' => 'POST'))); ?>
                            <fieldset>
                                <input type="hidden" name="action" value="increase"/>
                                <input type="hidden" name="id_of_person" value="<?php echo $lecturer->id_of_person; ?>"/>
                                <input type="hidden" name="id_of_room" value="<?php echo $id_of_room; ?>"/>
                                <div class="form-actions">
                                    <?php echo $this->tag->submitButton(array('UP', 'class' => 'btn btn-primary btn-xs')); ?>
                                </div>
                            </fieldset>
                            </form>
                        </div>
                    <?php } else { ?>
                        <div class="col-xs-1">
                            <?php echo $this->tag->form(array('rooms/modify', array('method' => 'POST'))); ?>
                            <fieldset>
                                <input type="hidden" name="action" value="increase"/>
                                <input type="hidden" name="id_of_person" value="<?php echo $lecturer->id_of_person; ?>"/>
                                <input type="hidden" name="id_of_room" value="<?php echo $id_of_room; ?>"/>
                                <div class="form-actions">
                                    <?php echo $this->tag->submitButton(array('UP', 'class' => 'btn btn-primary btn-xs')); ?>
                                </div>
                            </fieldset>
                            </form>
                        </div>
                        <div class="col-xs-1">
                            <?php echo $this->tag->form(array('rooms/modify', array('method' => 'POST'))); ?>
                            <fieldset>
                                <input type="hidden" name="action" value="decrease"/>
                                <input type="hidden" name="id_of_person" value="<?php echo $lecturer->id_of_person; ?>"/>
                                <input type="hidden" name="id_of_room" value="<?php echo $id_of_room; ?>"/>
                                <div class="form-actions">
                                    <?php echo $this->tag->submitButton(array('DOWN', 'class' => 'btn btn-danger btn-xs')); ?>
                                </div>
                            </fieldset>
                            </form>
                        </div>
                    <?php } ?>
                <?php } ?>
            </td>
            <?php } ?>
        </tr>
        <?php if ($v15690813561loop->last) { ?>
            </table>
        <?php } ?>
    <?php $v15690813561incr++; } ?>

    <?php if ($isValidToAdd) { ?>
        <div id="buttons" style="text-align: right">
            <a href="add?id_of_room=<?php echo $id_of_room; ?>&type=Lecturer" class="btn btn-primary smallMargin" id="addLecturerButton">ADD</a>
        </div>
        <br>
    <?php } ?>
</div>

<div id="staffDiv">
    <h3>Staffs</h3>
    <?php $v15690813561iterator = $staffs; $v15690813561incr = 0; $v15690813561loop = new stdClass(); $v15690813561loop->length = count($v15690813561iterator); $v15690813561loop->index = 1; $v15690813561loop->index0 = 1; $v15690813561loop->revindex = $v15690813561loop->length; $v15690813561loop->revindex0 = $v15690813561loop->length - 1; ?><?php foreach ($v15690813561iterator as $staff) { ?><?php $v15690813561loop->first = ($v15690813561incr == 0); $v15690813561loop->index = $v15690813561incr + 1; $v15690813561loop->index0 = $v15690813561incr; $v15690813561loop->revindex = $v15690813561loop->length - $v15690813561incr; $v15690813561loop->revindex0 = $v15690813561loop->length - ($v15690813561incr + 1); $v15690813561loop->last = ($v15690813561incr == ($v15690813561loop->length - 1)); ?>
        <?php if ($v15690813561loop->first) { ?>
            <table class="table">
            <tr>
                <th>Num</th>
                <th>Name</th>
                <th>Status</th>
                <th>Last Update</th>
                <?php if ($isValidToAdd && $this->length($staffs) > 1) { ?>
                    <th>Order</th>
                <?php } ?>
            </tr>
        <?php } ?>
        <tr>
            <td><?php echo $staff->room_order; ?></td>
            <td><?php echo $staff->name; ?></td>
            <td><?php echo $staff->status; ?></td>
            <td><?php echo $staff->last_update; ?></td>
            <?php if ($this->length($staffs) > 1) { ?>
            <td>
                <?php if ($isValidToAdd) { ?>
                    <?php if ($v15690813561loop->first) { ?>
                        <div class="col-xs-1">
                            <?php echo $this->tag->form(array('rooms/modify', array('method' => 'POST'))); ?>
                            <fieldset>
                                <input type="hidden" name="action" value="decrease"/>
                                <input type="hidden" name="id_of_person" value="<?php echo $staff->id_of_person; ?>"/>
                                <input type="hidden" name="id_of_room" value="<?php echo $id_of_room; ?>"/>
                                <div class="form-actions">
                                    <?php echo $this->tag->submitButton(array('DOWN', 'class' => 'btn btn-danger btn-xs')); ?>
                                </div>
                            </fieldset>
                            </form>
                        </div>
                    <?php } elseif ($v15690813561loop->last) { ?>
                        <div class="col-xs-1">
                            <?php echo $this->tag->form(array('rooms/modify', array('method' => 'POST'))); ?>
                            <fieldset>
                                <input type="hidden" name="action" value="increase"/>
                                <input type="hidden" name="id_of_person" value="<?php echo $staff->id_of_person; ?>"/>
                                <input type="hidden" name="id_of_room" value="<?php echo $id_of_room; ?>"/>
                                <div class="form-actions">
                                    <?php echo $this->tag->submitButton(array('UP', 'class' => 'btn btn-primary btn-xs')); ?>
                                </div>
                            </fieldset>
                            </form>
                        </div>
                    <?php } else { ?>
                        <div class="col-xs-1">
                            <?php echo $this->tag->form(array('rooms/modify', array('method' => 'POST'))); ?>
                            <fieldset>
                                <input type="hidden" name="action" value="increase"/>
                                <input type="hidden" name="id_of_person" value="<?php echo $staff->id_of_person; ?>"/>
                                <input type="hidden" name="id_of_room" value="<?php echo $id_of_room; ?>"/>
                                <div class="form-actions">
                                    <?php echo $this->tag->submitButton(array('UP', 'class' => 'btn btn-primary btn-xs')); ?>
                                </div>
                            </fieldset>
                            </form>
                        </div>
                        <div class="col-xs-1">
                            <?php echo $this->tag->form(array('rooms/modify', array('method' => 'POST'))); ?>
                            <fieldset>
                                <input type="hidden" name="action" value="decrease"/>
                                <input type="hidden" name="id_of_person" value="<?php echo $staff->id_of_person; ?>"/>
                                <input type="hidden" name="id_of_room" value="<?php echo $id_of_room; ?>"/>
                                <div class="form-actions">
                                    <?php echo $this->tag->submitButton(array('DOWN', 'class' => 'btn btn-danger btn-xs')); ?>
                                </div>
                            </fieldset>
                            </form>
                        </div>
                    <?php } ?>
                <?php } ?>
            </td>
            <?php } ?>
        </tr>
        <?php if ($v15690813561loop->last) { ?>
            </table>
        <?php } ?>
    <?php $v15690813561incr++; } ?>

    <?php if ($isValidToAdd) { ?>
        <div id="buttons" style="text-align: right">
            <a href="add?id_of_room=<?php echo $id_of_room; ?>&type=Staff" class="btn btn-primary smallMargin" id="addStaffButton">ADD</a>
        </div>
        <br>
    <?php } ?>
</div>

<div id="guestDiv">
    <h3>Guests</h3>
    <?php $v15690813561iterator = $guests; $v15690813561incr = 0; $v15690813561loop = new stdClass(); $v15690813561loop->length = count($v15690813561iterator); $v15690813561loop->index = 1; $v15690813561loop->index0 = 1; $v15690813561loop->revindex = $v15690813561loop->length; $v15690813561loop->revindex0 = $v15690813561loop->length - 1; ?><?php foreach ($v15690813561iterator as $guest) { ?><?php $v15690813561loop->first = ($v15690813561incr == 0); $v15690813561loop->index = $v15690813561incr + 1; $v15690813561loop->index0 = $v15690813561incr; $v15690813561loop->revindex = $v15690813561loop->length - $v15690813561incr; $v15690813561loop->revindex0 = $v15690813561loop->length - ($v15690813561incr + 1); $v15690813561loop->last = ($v15690813561incr == ($v15690813561loop->length - 1)); ?>
        <?php if ($v15690813561loop->first) { ?>
            <table class="table">
            <tr>
                <th>Name</th>
                <th>Type</th>
                <th>Last Update</th>
                <th>Action</th>
            </tr>
        <?php } ?>
        <tr>
            <td><?php echo $guest->name; ?></td>
            <td><?php echo $guest->type; ?></td>
            <td><?php echo $guest->last_update; ?></td>
            <td>
                <a>DELETE</a>
            </td>
        </tr>
        <?php if ($v15690813561loop->last) { ?>
            </table>
        <?php } ?>
    <?php $v15690813561incr++; } ?>
</div>

<?php echo $this->tag->stylesheetLink('css/register/show.css'); ?>