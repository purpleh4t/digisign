<?php echo $this->getContent(); ?>
<div class="jumbotron">
    <h1>Welcome to DigiSign</h1>
    <p>DigiSign is a revolutionary portal to manage digital signage in Universitas Gadjah Mada</p>
    <p><?php echo $this->tag->linkTo(array('register', 'Register now &raquo;', 'class' => 'btn btn-primary btn-large btn-success')); ?></p>
</div>

