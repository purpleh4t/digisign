<?php echo $this->getContent(); ?>

<?php echo $this->headerElements->getTabs(); ?>

<h1><?php echo $title; ?></h1>

<?php echo $this->tag->form(array('rooms/add', 'id' => 'searchStaffForm', 'method' => 'GET')); ?>
<fieldset>
    <div class="control-group">
        <?php echo $searchStaffForm->label('keywords', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $searchStaffForm->render('keywords', array('class' => 'form-control')); ?>
            <p class="help-block">ID or Name</p>
        </div>
    </div>

    <?php echo $searchStaffForm->render('id_of_room'); ?>
    <?php echo $searchStaffForm->render('type'); ?>
    <div class="form-actions" style="margin-bottom: 10px; text-align: right;">
        <?php echo $this->tag->submitButton(array('Search', 'class' => 'btn btn-primary')); ?>
    </div>
</fieldset>
</form>

<?php $v8867021321iterator = $staffs; $v8867021321incr = 0; $v8867021321loop = new stdClass(); $v8867021321loop->length = count($v8867021321iterator); $v8867021321loop->index = 1; $v8867021321loop->index0 = 1; $v8867021321loop->revindex = $v8867021321loop->length; $v8867021321loop->revindex0 = $v8867021321loop->length - 1; ?><?php foreach ($v8867021321iterator as $staff) { ?><?php $v8867021321loop->first = ($v8867021321incr == 0); $v8867021321loop->index = $v8867021321incr + 1; $v8867021321loop->index0 = $v8867021321incr; $v8867021321loop->revindex = $v8867021321loop->length - $v8867021321incr; $v8867021321loop->revindex0 = $v8867021321loop->length - ($v8867021321incr + 1); $v8867021321loop->last = ($v8867021321incr == ($v8867021321loop->length - 1)); ?>
    <?php if ($v8867021321loop->first) { ?>
        <table class="table table-hover table-responsive">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Current Room</th>
        </tr>
    <?php } ?>
    <tr data-id_of_staff=<?php echo $staff->id_of_staff; ?> class="clickable-row">
        <td><?php echo $staff->id_of_staff; ?></td>
        <td><?php echo $staff->name; ?></td>
        <td><?php echo $staff->current_room; ?></td>
    </tr>
    <?php if ($v8867021321loop->last) { ?>
        </table>
        <p style="text-align: right;">Note : Click the row to choose</p>
    <?php } ?>
<?php $v8867021321incr++; } ?>

<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Attention</h4>
            </div>
            <div class="modal-body">
                <p>You are going to make changes. Please make sure the following data is correct.</p>
                <div id="modal-form">
                    <?php if ($moveStaffForm) { ?>
                        <?php echo $this->tag->form(array('rooms/add', 'method' => 'POST')); ?>
                            <fieldset>
                                <div class="control-group">
                                    <?php echo $moveStaffForm->label('id_of_staff', array('class' => 'control-label')); ?>
                                    <div class="controls">
                                        <?php echo $moveStaffForm->render('id_of_staff', array('class' => 'form-control', 'readonly' => 'readonly')); ?>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <?php echo $moveStaffForm->label('name', array('class' => 'control-label')); ?>
                                    <div class="controls">
                                        <?php echo $moveStaffForm->render('name', array('class' => 'form-control', 'readonly' => 'readonly')); ?>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <?php echo $moveStaffForm->label('from_name', array('class' => 'control-label')); ?>
                                    <div class="controls">
                                        <?php echo $moveStaffForm->render('from_name', array('class' => 'form-control', 'readonly' => 'readonly')); ?>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <?php echo $moveStaffForm->label('to_name', array('class' => 'control-label')); ?>
                                    <div class="controls">
                                        <?php echo $moveStaffForm->render('to_name', array('class' => 'form-control', 'readonly' => 'readonly')); ?>
                                    </div>
                                </div>
                                <?php echo $moveStaffForm->render('id_of_room'); ?>
                                <?php echo $this->tag->submitButton(array('Submit', 'class' => 'btn btn-info pull-right', 'style' => 'text-align:right; margin-top:10px;')); ?>
                            </fieldset>
                        </form>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo $this->tag->javascriptInclude('js/rooms/add.js'); ?>