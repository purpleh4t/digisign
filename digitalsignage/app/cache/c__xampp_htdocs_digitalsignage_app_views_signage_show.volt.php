<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php echo $this->tag->stylesheetLink('css/bootstrap.min.css'); ?>
    <?php echo $this->tag->stylesheetLink('css/signage/show.css'); ?>
    <?php echo $this->tag->javascriptInclude('js/jquery.min.js'); ?>
    <?php echo $this->tag->javascriptInclude('js/bootstrap.min.js'); ?>
</head>
<body>
<div class="header">
    <div class="row">
        <div class="col-sm-3">
            <img src="../img/logo-ugm.png" class="center-block"/>
        </div>
        <div class="col-sm-6">
            <div class="center-block">
                <h4 id="room-name" class="header-text"><?php echo $roomName; ?></h4>
                <h4 id="department-name" class="header-text"><?php echo $departmentName; ?></h4>
            </div>
        </div>
        <div class="col-sm-3">

        </div>
    </div>
</div>
<div class="container">
    <?php echo $this->getContent(); ?>
</div>
<div id="footer">
    <hr>
    <p id="footer-font">© 2016 Copyright Smart System Research Group, Universitas Gadjah Mada</p>
</div>
</body>
</html>
