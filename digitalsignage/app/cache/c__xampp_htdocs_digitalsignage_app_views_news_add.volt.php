<?php echo $this->getContent(); ?>

<?php echo $this->headerElements->getTabs(); ?>

<h1>Create a News</h1>

<?php echo $this->tag->form(array('news/add', 'id' => 'addNewsForm', 'method' => 'POST')); ?>
<fieldset>
    <div class="control-group">
        <?php echo $form->label('title', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->render('title', array('class' => 'form-control')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->label('content', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->render('content', array('class' => 'form-control')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->label('image', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->render('image', array('class' => 'form-control')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->label('startTime', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->render('startTime', array('class' => 'form-control datepicker ')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->label('endTime', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->render('endTime', array('class' => 'form-control datepicker')); ?>
        </div>
    </div>

    <?php echo $form->render('room'); ?>

    <div class="form-actions" style="margin-top:10px;">
        <?php echo $this->tag->submitButton(array('Save', 'class' => 'btn btn-primary')); ?>
        <p class="help-block">Please double check your data before save</p>
    </div>
</fieldset>