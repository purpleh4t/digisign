<?php echo $this->getContent(); ?>
<div class="page-header">
    <h2>Register for DigiSign</h2>
</div>

<?php echo $this->tag->form(array('register', 'id' => 'registerForm', 'onbeforesubmit' => 'return false')); ?>
<fieldset>
    <div class="control-group">
        <?php echo $form->label('id_of_person', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->render('id_of_person', array('class' => 'form-control')); ?>
            <p class="help-block">(required)</p>
            <div class="alert alert-warning" id="id_of_person_alert">
                <strong>Warning!</strong> Please enter your identity number
            </div>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->label('name', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->render('name', array('class' => 'form-control')); ?>
            <p class="help-block">(required)</p>
            <div class="alert alert-warning" id="name_alert">
                <strong>Warning!</strong> Please enter your full name
            </div>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->label('type', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->render('type', array('class' => 'form-control', 'onchange' => 'staffChecks();')); ?>
            <p class="help-block">(required)</p>
            <div class="alert alert-warning" id="type_alert">
                <strong>Warning!</strong> Please choose account type
            </div>
        </div>
    </div>

    <div id="staffForm">
        <div class="control-group">
            <?php echo $form->label('email', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->render('email', array('class' => 'form-control')); ?>
                <p class="help-block">(required)</p>
                <div class="alert alert-warning" id="email_alert">
                    <strong>Warning!</strong> Please enter your email address
                </div>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->label('password', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->render('password', array('class' => 'form-control')); ?>
                <p class="help-block">(minimum 8 characters)</p>
                <div class="alert alert-warning" id="password_alert">
                    <strong>Warning!</strong> Please provide a valid password
                </div>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="repeatPassword">Repeat Password</label>
            <div class="controls">
                <?php echo $this->tag->passwordField(array('repeatPassword', 'class' => 'form-control')); ?>
                <p class="help-block">(required)</p>
                <div class="alert alert-warning" id="repeatPassword_alert">
                    <strong>Warning!</strong> The password does not match
                </div>
            </div>
        </div>

        <?php echo $roomElements->getRoomSelection(); ?>
        <?php echo $this->tag->javascriptInclude('js/roomselector.js'); ?>
    </div>

    <div class="control-group">
        <?php echo $form->label('description', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->render('description', array('class' => 'form-control')); ?>
            <p class="help-block">(required)</p>
            <div class="alert alert-warning" id="description_alert">
                <strong>Warning!</strong> Please describe about your position
            </div>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->label('rfid', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->render('rfid', array('class' => 'form-control')); ?>
            <p class="help-block">(required)</p>
            <div class="alert alert-warning" id="rfid_alert">
                <strong>Warning!</strong> Please enter your RFID tag
            </div>
        </div>
    </div>

    <div class="form-actions">
        <?php echo $this->tag->submitButton(array('Register', 'class' => 'btn btn-primary', 'onclick' => 'return FormElement.validate();')); ?>
        <p class="help-block">By signing up, you accept terms of use and privacy policy.</p>
    </div>
</fieldset>
</form>

<?php echo $this->tag->javascriptInclude('js/register/register.js'); ?>