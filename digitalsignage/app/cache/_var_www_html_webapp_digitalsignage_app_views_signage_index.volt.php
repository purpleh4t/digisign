<?php echo $this->getContent(); ?>

<?php echo $this->tag->stylesheetLink('css/rooms/main.css'); ?>

<div id="roomSelectorDiv">
    <div class="well">
        <h1>Choose a Room Signage</h1>
        <?php echo $this->tag->form(array('signage/show', 'method' => 'GET')); ?>
        <fieldset>
            <?php echo $roomElements->getRoomSelection(); ?>
            <?php echo $this->tag->javascriptInclude('js/roomselector.js'); ?>
            <div class="form-actions">
                <?php echo $this->tag->submitButton(array('Select', 'id' => 'selectButton', 'class' => 'btn btn-info pull-right')); ?>
            </div>
        </fieldset>
        </form>
    </div>
</div>