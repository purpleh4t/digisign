<?php echo $this->getContent(); ?>

<?php echo $this->headerElements->getTabs(); ?>

<h1><?php echo $title; ?></h1>

<div id="currentNewsDiv">
    <h3>Current News</h3>
    <?php if ($news) { ?>
        <?php $v28669818171iterator = $news; $v28669818171incr = 0; $v28669818171loop = new stdClass(); $v28669818171loop->length = count($v28669818171iterator); $v28669818171loop->index = 1; $v28669818171loop->index0 = 1; $v28669818171loop->revindex = $v28669818171loop->length; $v28669818171loop->revindex0 = $v28669818171loop->length - 1; ?><?php foreach ($v28669818171iterator as $aNews) { ?><?php $v28669818171loop->first = ($v28669818171incr == 0); $v28669818171loop->index = $v28669818171incr + 1; $v28669818171loop->index0 = $v28669818171incr; $v28669818171loop->revindex = $v28669818171loop->length - $v28669818171incr; $v28669818171loop->revindex0 = $v28669818171loop->length - ($v28669818171incr + 1); $v28669818171loop->last = ($v28669818171incr == ($v28669818171loop->length - 1)); ?>
            <?php if ($v28669818171loop->first) { ?>
                <table class="table table-responsive">
                <tr>
                    <th>Author</th>
                    <th>Title</th>
                    <th>Start Time</th>
                    <th>End Time</th>
                    <th>Action</th>
                </tr>
            <?php } ?>
            <tr data-id-of-person="<?php echo $aNews->id_of_person; ?>" data-id-of-news="<?php echo $aNews->id_of_news; ?>" data-content="<?php echo $aNews->content; ?>" data-image-link="<?php echo $aNews->imageLink; ?>">
                <td><?php echo $aNews->author; ?></td>
                <td><?php echo $aNews->title; ?></td>
                <td><?php echo $aNews->startTime; ?></td>
                <td><?php echo $aNews->endTime; ?></td>
                <td>
                    <button style="margin: 2px;" class="btn btn-info btn-xs btn-view">View</button>
                    <?php if ($session_id_of_person == $aNews->id_of_person) { ?>
                        <button style="margin: 2px;" class="btn btn-warning btn-xs btn-edit">Edit</button>
                        <button style="margin: 2px;" class="btn btn-danger btn-xs btn-delete">Delete</button>
                    <?php } ?>
                </td>
            </tr>
            <?php if ($v28669818171loop->last) { ?>
                </table>
            <?php } ?>
        <?php $v28669818171incr++; } ?>
    <?php } else { ?>
        <p>There is no active news in this room</p>
    <?php } ?>
    <?php if ($validToAdd) { ?>
        <?php echo $this->tag->form(array('news/add', 'method' => 'get')); ?>
        <fieldset>
            <?php echo $dateForm->render('room', array('value' => $room)); ?>
            <div style="text-align: right;">
                <?php echo $this->tag->submitButton(array('ADD', 'class' => 'btn btn-primary', 'style' => 'margin-top:10px;')); ?>
            </div>
        </fieldset>
        </form>
    <?php } ?>
</div>

<div id="editModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="edit-modal-title">TEXT</h4>
            </div>
            <div class="modal-body">
                <div id="modal-form">
                    <?php echo $this->tag->form(array('news/show', 'id' => 'formEdit', 'method' => 'POST')); ?>
                    <fieldset>
                        <div class="control-group">
                            <?php echo $editNewsForm->label('title', array('class' => 'control-label')); ?>
                            <div class="controls">
                                <?php echo $editNewsForm->render('title', array('class' => 'form-control', 'readonly' => 'readonly')); ?>
                            </div>
                        </div>

                        <div class="control-group">
                            <?php echo $editNewsForm->label('content', array('class' => 'control-label')); ?>
                            <div class="controls">
                                <?php echo $editNewsForm->render('content', array('class' => 'form-control', 'readonly' => 'readonly')); ?>
                            </div>
                        </div>

                        

                        <div class="control-group">
                            <?php echo $editNewsForm->label('startTime', array('class' => 'control-label')); ?>
                            <div class="controls">
                                <?php echo $editNewsForm->render('startTime', array('class' => 'form-control datepicker', 'readonly' => 'readonly')); ?>
                            </div>
                        </div>

                        <div class="control-group">
                            <?php echo $editNewsForm->label('endTime', array('class' => 'control-label')); ?>
                            <div class="controls">
                                <?php echo $editNewsForm->render('endTime', array('class' => 'form-control datepicker', 'readonly' => 'readonly')); ?>
                            </div>
                        </div>

                        <?php echo $editNewsForm->render('room'); ?>
                        <?php echo $this->tag->hiddenField(array('idOfNews')); ?>
                        <?php echo $this->tag->hiddenField(array('author')); ?>
                        <?php echo $this->tag->hiddenField(array('action')); ?>
                        <div style="text-align : right">
                            <?php echo $this->tag->submitButton(array('SAVE', 'id' => 'editButton', 'class' => 'btn btn-warning', 'style' => 'margin-top:10px;')); ?>
                        </div>
                    </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="viewModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="view-modal-title">TEXT</h4>
            </div>
            <div class="modal-body">
                <p id="view-content"></p>
                <p id="view-author"></p>
                <div id="view-start-time"></div>
                <div id="view-end-time"></div>
            </div>
        </div>
    </div>
</div>

<div id="outdatedNewsDiv">
    <h3>Outdated News</h3>
    <?php echo $this->tag->form(array('news/show', 'id' => 'searchNewsForm', 'method' => 'get')); ?>
        <fieldset>
            <div class="control-group col-sm-6">
                <?php echo $dateForm->label('dateFrom', array('class' => 'control-label')); ?>
                <div class="controls">
                    <?php echo $dateForm->render('dateFrom', array('class' => 'form-control')); ?>
                </div>
            </div>

            <div class="control-group col-sm-6">
                <?php echo $dateForm->label('dateTo', array('class' => 'control-label')); ?>
                <div class="controls">
                    <?php echo $dateForm->render('dateTo', array('class' => 'form-control')); ?>
                </div>
            </div>

            <?php echo $dateForm->render('room', array('value' => $room)); ?>

            <div class="form-actions" style="text-align: right;">
                <?php echo $this->tag->submitButton(array('Search', 'class' => 'btn btn-info', 'style' => 'margin-top : 10px;')); ?>
            </div>
        </fieldset>
    </form>

    <?php if ($newsLogs) { ?>
        <?php $v28669818171iterator = $newsLogs; $v28669818171incr = 0; $v28669818171loop = new stdClass(); $v28669818171loop->length = count($v28669818171iterator); $v28669818171loop->index = 1; $v28669818171loop->index0 = 1; $v28669818171loop->revindex = $v28669818171loop->length; $v28669818171loop->revindex0 = $v28669818171loop->length - 1; ?><?php foreach ($v28669818171iterator as $aNews) { ?><?php $v28669818171loop->first = ($v28669818171incr == 0); $v28669818171loop->index = $v28669818171incr + 1; $v28669818171loop->index0 = $v28669818171incr; $v28669818171loop->revindex = $v28669818171loop->length - $v28669818171incr; $v28669818171loop->revindex0 = $v28669818171loop->length - ($v28669818171incr + 1); $v28669818171loop->last = ($v28669818171incr == ($v28669818171loop->length - 1)); ?>
            <?php if ($v28669818171loop->first) { ?>
                <table class="table table-responsive">
                <tr>
                    <th>Author</th>
                    <th>Title</th>
                    <th>Start Time</th>
                    <th>End Time</th>
                    <th>Action</th>
                </tr>
            <?php } ?>
            <tr data-id-of-person="<?php echo $aNews->id_of_person; ?>" data-id-of-news="<?php echo $aNews->id_of_news; ?>" data-content="<?php echo $aNews->content; ?>" data-image-link="<?php echo $aNews->imageLink; ?>">
                <td><?php echo $aNews->author; ?></td>
                <td><?php echo $aNews->title; ?></td>
                <td><?php echo $aNews->startTime; ?></td>
                <td><?php echo $aNews->endTime; ?></td>
                <td>
                    <button style="margin: 2px;" class="btn btn-info btn-xs btn-view">View</button>
                    <?php if ($session_id_of_person == $aNews->id_of_person) { ?>
                        <button style="margin: 2px;" class="btn btn-warning btn-xs btn-edit">Edit</button>
                        <button style="margin: 2px;" class="btn btn-danger btn-xs btn-delete">Delete</button>
                    <?php } ?>
                </td>
            </tr>
            <?php if ($v28669818171loop->last) { ?>
                </table>
            <?php } ?>
        <?php $v28669818171incr++; } ?>
    <?php } ?>
</div>

<?php echo $this->tag->javascriptInclude('js/news/show.js'); ?>