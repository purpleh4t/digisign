<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{ stylesheet_link('css/bootstrap.min.css') }}
    {{ stylesheet_link('css/signage/show.css') }}
    {{ javascript_include('js/jquery.min.js') }}
    {{ javascript_include('js/bootstrap.min.js') }}
</head>
<body>
<div class="header">
    <div class="row">
        <div class="col-sm-3">
            <img src="../img/logo-ugm.png" class="center-block"/>
        </div>
        <div class="col-sm-6">
            <div class="center-block">
                <h4 id="room-name" class="header-text">{{ roomName }}</h4>
                <h4 id="department-name" class="header-text">{{ departmentName }}</h4>
            </div>
        </div>
        <div class="col-sm-3">

        </div>
    </div>
</div>
<div class="container">
    {{ content() }}
    <div>
        <h4>Lecturers</h4>
        {% if lecturers %}

        {% endif %}
    </div>
    <h4>Staffs</h4>
    <h4>Students</h4>
</div>
<div id="footer">
    <hr>
    <p id="footer-font">© 2016 Copyright Smart System Research Group, Universitas Gadjah Mada</p>
</div>
</body>
</html>
