{{ content() }}

{{ headerElements.getTabs() }}

<h1>{{ title }}</h1>

{{ form('rooms/add', 'id': 'searchStaffForm', 'method': 'GET') }}
<fieldset>
    <div class="control-group">
        {{ searchStaffForm.label('keywords', ['class': 'control-label']) }}
        <div class="controls">
            {{ searchStaffForm.render('keywords', ['class': 'form-control']) }}
            <p class="help-block">ID or Name</p>
        </div>
    </div>

    {{ searchStaffForm.render('id_of_room') }}
    {{ searchStaffForm.render('type') }}
    <div class="form-actions" style="margin-bottom: 10px; text-align: right;">
        {{ submit_button('Search', 'class': 'btn btn-primary') }}
    </div>
</fieldset>
</form>

{% for staff in staffs %}
    {% if loop.first %}
        <table class="table table-hover table-responsive">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Current Room</th>
        </tr>
    {% endif %}
    <tr data-id_of_staff={{ staff.id_of_person }} class="clickable-row">
        <td>{{ staff.id_of_person }}</td>
        <td>{{ staff.name }}</td>
        <td>{{ staff.current_room }}</td>
    </tr>
    {% if loop.last %}
        </table>
        <p style="text-align: right;">Note : Click the row to choose</p>
    {% endif %}
{% endfor %}

<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Attention</h4>
            </div>
            <div class="modal-body">
                <p>You are going to make changes. Please make sure the following data is correct.</p>
                <div id="modal-form">
                    {% if moveStaffForm %}
                        {{ form("rooms/add", 'method':"POST") }}
                            <fieldset>
                                <div class="control-group">
                                    {{ moveStaffForm.label('id_of_person', ['class': 'control-label']) }}
                                    <div class="controls">
                                        {{ moveStaffForm.render('id_of_person', ['class': 'form-control', 'readonly': 'readonly']) }}
                                    </div>
                                </div>
                                <div class="control-group">
                                    {{ moveStaffForm.label('name', ['class': 'control-label']) }}
                                    <div class="controls">
                                        {{ moveStaffForm.render('name', ['class': 'form-control', 'readonly': 'readonly']) }}
                                    </div>
                                </div>
                                <div class="control-group">
                                    {{ moveStaffForm.label('from_name', ['class': 'control-label']) }}
                                    <div class="controls">
                                        {{ moveStaffForm.render('from_name', ['class': 'form-control', 'readonly': 'readonly']) }}
                                    </div>
                                </div>
                                <div class="control-group">
                                    {{ moveStaffForm.label('to_name', ['class': 'control-label']) }}
                                    <div class="controls">
                                        {{ moveStaffForm.render('to_name', ['class': 'form-control', 'readonly': 'readonly']) }}
                                    </div>
                                </div>
                                {{ moveStaffForm.render('id_of_room') }}
                                {{ submit_button('Submit', 'class': 'btn btn-info pull-right', 'style': 'text-align:right; margin-top:10px;') }}
                            </fieldset>
                        </form>
                    {% endif %}
                </div>
            </div>
        </div>
    </div>
</div>

{{ javascript_include("js/rooms/add.js") }}