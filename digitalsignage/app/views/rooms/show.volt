{{ content() }}

{{ headerElements.getTabs() }}

<h1>{{ title }}</h1>

<div id="lecturerDiv">
    <h3>Lecturers</h3>
    {% for lecturer in lecturers %}
        {% if loop.first %}
            <table class="table">
            <tr>
                <th>Num</th>
                <th>Name</th>
                <th>Status</th>
                <th>Last Update</th>
                {% if isValidToAdd AND lecturers|length > 1 %}
                    <th>Order</th>
                {% endif %}
            </tr>
        {% endif %}
        <tr>
            <td>{{ lecturer.room_order }}</td>
            <td>{{ lecturer.name }}</td>
            <td>{{ lecturer.status }}</td>
            <td>{{ lecturer.last_update }}</td>
            {% if lecturers|length > 1 %}
            <td>
                {% if isValidToAdd  %}
                    {% if loop.first %}
                        <div class="col-xs-1">
                            {{ form('rooms/modify', {'method':'POST'}) }}
                            <fieldset>
                                <input type="hidden" name="action" value="decrease"/>
                                <input type="hidden" name="id_of_person" value="{{ lecturer.id_of_person }}"/>
                                <input type="hidden" name="id_of_room" value="{{ id_of_room }}"/>
                                <div class="form-actions">
                                    {{ submit_button('DOWN', 'class':'btn btn-danger btn-xs') }}
                                </div>
                            </fieldset>
                            </form>
                        </div>
                    {% elseif loop.last %}
                        <div class="col-xs-1">
                            {{ form('rooms/modify', {'method':'POST'}) }}
                            <fieldset>
                                <input type="hidden" name="action" value="increase"/>
                                <input type="hidden" name="id_of_person" value="{{ lecturer.id_of_person }}"/>
                                <input type="hidden" name="id_of_room" value="{{ id_of_room }}"/>
                                <div class="form-actions">
                                    {{ submit_button('UP', 'class':'btn btn-primary btn-xs') }}
                                </div>
                            </fieldset>
                            </form>
                        </div>
                    {% else %}
                        <div class="col-xs-1">
                            {{ form('rooms/modify', {'method':'POST'}) }}
                            <fieldset>
                                <input type="hidden" name="action" value="increase"/>
                                <input type="hidden" name="id_of_person" value="{{ lecturer.id_of_person }}"/>
                                <input type="hidden" name="id_of_room" value="{{ id_of_room }}"/>
                                <div class="form-actions">
                                    {{ submit_button('UP', 'class':'btn btn-primary btn-xs') }}
                                </div>
                            </fieldset>
                            </form>
                        </div>
                        <div class="col-xs-1">
                            {{ form('rooms/modify', {'method':'POST'}) }}
                            <fieldset>
                                <input type="hidden" name="action" value="decrease"/>
                                <input type="hidden" name="id_of_person" value="{{ lecturer.id_of_person }}"/>
                                <input type="hidden" name="id_of_room" value="{{ id_of_room }}"/>
                                <div class="form-actions">
                                    {{ submit_button('DOWN', 'class':'btn btn-danger btn-xs') }}
                                </div>
                            </fieldset>
                            </form>
                        </div>
                    {% endif %}
                {% endif %}
            </td>
            {% endif %}
        </tr>
        {% if loop.last %}
            </table>
        {% endif %}
    {% endfor %}

    {% if isValidToAdd %}
        <div id="buttons" style="text-align: right">
            <a href="add?id_of_room={{ id_of_room }}&type=Lecturer" class="btn btn-primary smallMargin" id="addLecturerButton">ADD</a>
        </div>
        <br>
    {% endif %}
</div>

<div id="staffDiv">
    <h3>Staffs</h3>
    {% for staff in staffs %}
        {% if loop.first %}
            <table class="table">
            <tr>
                <th>Num</th>
                <th>Name</th>
                <th>Status</th>
                <th>Last Update</th>
                {% if isValidToAdd AND staffs|length >1 %}
                    <th>Order</th>
                {% endif %}
            </tr>
        {% endif %}
        <tr>
            <td>{{ staff.room_order }}</td>
            <td>{{ staff.name }}</td>
            <td>{{ staff.status }}</td>
            <td>{{ staff.last_update }}</td>
            {% if staffs|length > 1 %}
            <td>
                {% if isValidToAdd  %}
                    {% if loop.first %}
                        <div class="col-xs-1">
                            {{ form('rooms/modify', {'method':'POST'}) }}
                            <fieldset>
                                <input type="hidden" name="action" value="decrease"/>
                                <input type="hidden" name="id_of_person" value="{{ staff.id_of_person }}"/>
                                <input type="hidden" name="id_of_room" value="{{ id_of_room }}"/>
                                <div class="form-actions">
                                    {{ submit_button('DOWN', 'class':'btn btn-danger btn-xs') }}
                                </div>
                            </fieldset>
                            </form>
                        </div>
                    {% elseif loop.last %}
                        <div class="col-xs-1">
                            {{ form('rooms/modify', {'method':'POST'}) }}
                            <fieldset>
                                <input type="hidden" name="action" value="increase"/>
                                <input type="hidden" name="id_of_person" value="{{ staff.id_of_person }}"/>
                                <input type="hidden" name="id_of_room" value="{{ id_of_room }}"/>
                                <div class="form-actions">
                                    {{ submit_button('UP', 'class':'btn btn-primary btn-xs') }}
                                </div>
                            </fieldset>
                            </form>
                        </div>
                    {% else %}
                        <div class="col-xs-1">
                            {{ form('rooms/modify', {'method':'POST'}) }}
                            <fieldset>
                                <input type="hidden" name="action" value="increase"/>
                                <input type="hidden" name="id_of_person" value="{{ staff.id_of_person }}"/>
                                <input type="hidden" name="id_of_room" value="{{ id_of_room }}"/>
                                <div class="form-actions">
                                    {{ submit_button('UP', 'class':'btn btn-primary btn-xs') }}
                                </div>
                            </fieldset>
                            </form>
                        </div>
                        <div class="col-xs-1">
                            {{ form('rooms/modify', {'method':'POST'}) }}
                            <fieldset>
                                <input type="hidden" name="action" value="decrease"/>
                                <input type="hidden" name="id_of_person" value="{{ staff.id_of_person }}"/>
                                <input type="hidden" name="id_of_room" value="{{ id_of_room }}"/>
                                <div class="form-actions">
                                    {{ submit_button('DOWN', 'class':'btn btn-danger btn-xs') }}
                                </div>
                            </fieldset>
                            </form>
                        </div>
                    {% endif %}
                {% endif %}
            </td>
            {% endif %}
        </tr>
        {% if loop.last %}
            </table>
        {% endif %}
    {% endfor %}

    {% if isValidToAdd %}
        <div id="buttons" style="text-align: right">
            <a href="add?id_of_room={{ id_of_room }}&type=Staff" class="btn btn-primary smallMargin" id="addStaffButton">ADD</a>
        </div>
        <br>
    {% endif %}
</div>

<div id="guestDiv">
    <h3>Guests</h3>
    {% for guest in guests %}
        {% if loop.first %}
            <table class="table">
            <tr>
                <th>Name</th>
                <th>Type</th>
                <th>Last Update</th>
                <th>Action</th>
            </tr>
        {% endif %}
        <tr>
            <td>{{ guest.name }}</td>
            <td>{{ guest.type }}</td>
            <td>{{ guest.last_update }}</td>
            <td>
                <a>DELETE</a>
            </td>
        </tr>
        {% if loop.last %}
            </table>
        {% endif %}
    {% endfor %}
</div>

{{ stylesheet_link("css/register/show.css") }}