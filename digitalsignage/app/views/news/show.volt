{{ content() }}

{{ headerElements.getTabs() }}

<h1>{{ title }}</h1>

<div id="currentNewsDiv">
    <h3>Current News</h3>
    {% if news %}
        {% for aNews in news  %}
            {% if loop.first %}
                <table class="table table-responsive">
                <tr>
                    <th>Author</th>
                    <th>Title</th>
                    <th>Start Time</th>
                    <th>End Time</th>
                    <th>Action</th>
                </tr>
            {% endif %}
            <tr data-id-of-person="{{ aNews.id_of_person }}" data-id-of-news="{{ aNews.id_of_news }}" data-content="{{ aNews.content }}" data-image-link="{{ aNews.imageLink }}">
                <td>{{ aNews.author }}</td>
                <td>{{ aNews.title }}</td>
                <td>{{ aNews.startTime }}</td>
                <td>{{ aNews.endTime}}</td>
                <td>
                    <button style="margin: 2px;" class="btn btn-info btn-xs btn-view">View</button>
                    {% if session_id_of_person == aNews.id_of_person %}
                        <button style="margin: 2px;" class="btn btn-warning btn-xs btn-edit">Edit</button>
                        <button style="margin: 2px;" class="btn btn-danger btn-xs btn-delete">Delete</button>
                    {% endif %}
                </td>
            </tr>
            {% if loop.last %}
                </table>
            {% endif %}
        {% endfor %}
    {% else %}
        <p>There is no active news in this room</p>
    {% endif %}
    {% if validToAdd %}
        {{ form("news/add", "method":"get") }}
        <fieldset>
            {{ dateForm.render('room', ['value':room]) }}
            <div style="text-align: right;">
                {{ submit_button("ADD", "class":"btn btn-primary", "style":"margin-top:10px;") }}
            </div>
        </fieldset>
        </form>
    {% endif %}
</div>

<div id="editModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="edit-modal-title">TEXT</h4>
            </div>
            <div class="modal-body">
                <div id="modal-form">
                    {{ form("news/show", 'id':'formEdit', 'method':'POST') }}
                    <fieldset>
                        <div class="control-group">
                            {{ editNewsForm.label('title', ['class': 'control-label']) }}
                            <div class="controls">
                                {{ editNewsForm.render('title', ['class': 'form-control', 'readonly': 'readonly']) }}
                            </div>
                        </div>

                        <div class="control-group">
                            {{ editNewsForm.label('content', ['class': 'control-label']) }}
                            <div class="controls">
                                {{ editNewsForm.render('content', ['class': 'form-control', 'readonly': 'readonly']) }}
                            </div>
                        </div>

                        {#<div class="control-group">
                            {{ editNewsForm.label('image', ['class': 'control-label']) }}
                            <div class="controls">
                                {{ editNewsForm.render('image', ['class': 'form-control', 'readonly': 'readonly']) }}
                            </div>
                        </div>#}

                        <div class="control-group">
                            {{ editNewsForm.label('startTime', ['class': 'control-label']) }}
                            <div class="controls">
                                {{ editNewsForm.render('startTime', ['class': 'form-control datepicker', 'readonly': 'readonly']) }}
                            </div>
                        </div>

                        <div class="control-group">
                            {{ editNewsForm.label('endTime', ['class': 'control-label']) }}
                            <div class="controls">
                                {{ editNewsForm.render('endTime', ['class': 'form-control datepicker', 'readonly': 'readonly']) }}
                            </div>
                        </div>

                        {{ editNewsForm.render("room") }}
                        {{ hidden_field("idOfNews") }}
                        {{ hidden_field("author") }}
                        {{ hidden_field("action") }}
                        <div style="text-align : right">
                            {{ submit_button("SAVE", 'id':'editButton', 'class':'btn btn-warning', 'style':'margin-top:10px;') }}
                        </div>
                    </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="viewModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="view-modal-title">TEXT</h4>
            </div>
            <div class="modal-body">
                <p id="view-content"></p>
                <p id="view-author"></p>
                <div id="view-start-time"></div>
                <div id="view-end-time"></div>
            </div>
        </div>
    </div>
</div>

<div id="outdatedNewsDiv">
    <h3>Outdated News</h3>
    {{ form('news/show', 'id': 'searchNewsForm', "method":"get") }}
        <fieldset>
            <div class="control-group col-sm-6">
                {{ dateForm.label('dateFrom', ['class': 'control-label']) }}
                <div class="controls">
                    {{ dateForm.render('dateFrom', ['class': 'form-control']) }}
                </div>
            </div>

            <div class="control-group col-sm-6">
                {{ dateForm.label('dateTo', ['class': 'control-label']) }}
                <div class="controls">
                    {{ dateForm.render('dateTo', ['class': 'form-control']) }}
                </div>
            </div>

            {{ dateForm.render('room', ['value':room]) }}

            <div class="form-actions" style="text-align: right;">
                {{ submit_button('Search', 'class': 'btn btn-info', 'style' : 'margin-top : 10px;') }}
            </div>
        </fieldset>
    </form>

    {% if newsLogs %}
        {% for aNews in newsLogs  %}
            {% if loop.first %}
                <table class="table table-responsive">
                <tr>
                    <th>Author</th>
                    <th>Title</th>
                    <th>Start Time</th>
                    <th>End Time</th>
                    <th>Action</th>
                </tr>
            {% endif %}
            <tr data-id-of-person="{{ aNews.id_of_person }}" data-id-of-news="{{ aNews.id_of_news }}" data-content="{{ aNews.content }}" data-image-link="{{ aNews.imageLink }}">
                <td>{{ aNews.author }}</td>
                <td>{{ aNews.title }}</td>
                <td>{{ aNews.startTime }}</td>
                <td>{{ aNews.endTime}}</td>
                <td>
                    <button style="margin: 2px;" class="btn btn-info btn-xs btn-view">View</button>
                    {% if session_id_of_person == aNews.id_of_person %}
                        <button style="margin: 2px;" class="btn btn-warning btn-xs btn-edit">Edit</button>
                        <button style="margin: 2px;" class="btn btn-danger btn-xs btn-delete">Delete</button>
                    {% endif %}
                </td>
            </tr>
            {% if loop.last %}
                </table>
            {% endif %}
        {% endfor %}
    {% endif %}
</div>

{{ javascript_include("js/news/show.js") }}