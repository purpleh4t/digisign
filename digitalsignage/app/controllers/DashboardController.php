<?php

class DashboardController extends ControllerBase
{
    public function initialize(){
        parent::initialize();
    }

    public function indexAction()
    {
        $this->forward('rooms/index');
    }

}

