<?php

class PresenceController extends ControllerBase{
    public function initialize(){
        parent::initialize();
        date_default_timezone_set('Asia/Jakarta');
    }

    public function indexAction(){
        $response = new \Phalcon\Http\Response();

        if ($this->request->hasQuery("id") && $this->request->hasQuery("tag")){
            $xBeeTag = $this->request->getQuery("id");
            $personTag = $this->request->getQuery("tag");

            $person = Person::findFirst(array("RFID = \"$personTag\""));
            $room = Room::findFirst(array("id_of_xbee = :id_of_xbee:", "bind"=>array("id_of_xbee"=>$xBeeTag)));

            $director = Director::findFirst(array("id_of_person = \"$person->id_of_person\""));
            $staff = Staff::findFirst(array("id_of_person = \"$person->id_of_person\""));
            $student = Student::findFirst(array("id_of_person = \"$person->id_of_person\""));

            $credential_id_of_room = array();
            if($director){
                $credential_id_of_room[] = $director->id_of_room;
            }elseif($staff){
                $credential_id_of_room[] = $staff->id_of_room;
            }else{
                $credential_id_of_room[] = $student->id_of_room;
            }

            // Make length $credential_id_of_room to be 2
            if(count($credential_id_of_room)==1){
                $credential_id_of_room[] = -1;
            }

            $returnCode = "00";

            if($person && $room){
                $activity = Activity::findFirst(array("id_of_person = \"$person->id_of_person\"", "order"=>"start_time DESC"));
                // Checked in
                if($person->last_checked_in == null || $activity->duration != "0"){
                    if($credential_id_of_room[0] == $room->id_of_room || $credential_id_of_room[1] == $room->id_of_room){
                        $result = $this->CheckIn($person, $room);
                        if(!$result["error"]){
                            $returnCode = "11";
                        }else{
                            $returnCode = "99";
                        }
                    }else{
                        $returnCode = "01";
                    }
                }
                // Checked out
                else{
                    if($room->id_of_room == $activity->id_of_room){
                        $result = $this->CheckOut($activity, $person, $room);
                        if(!$result["error"]){
                            $returnCode = "10";
                        }else{
                            $returnCode = "99";
                        }
                    }else{
                        $returnCode = "01";
                    }
                }
            }

            $response->setContent("$returnCode");
        }

        return $response;
    }

    private function CheckIn($person, $room){
        $now = date('Y-m-d H:i:s', time());

        $activity = new Activity();
        $activity->id_of_room = $room->id_of_room;
        $activity->id_of_person = $person->id_of_person;
        $activity->start_time = $now;
        $activity->end_time = $now;
        $activity->duration = 0;
        if($activity->create()){
            $person->last_checked_in = $activity->start_time;
            $person->save();
            $error = false;
            $message = $person->name . " has been successfully checked in of " . $room->name . " at " . $activity->start_time;
        }else{
            $error = true;
            $message = "Error when recording activity in database 2";
        }
        return array("error"=>$error, "message"=>$message);
    }

    private function CheckOut($activity, $person, $room){
        if(parent::isToday($activity->start_time)){
            $activity->end_time = date('Y-m-d H:i:s', time());
            $duration = parent::interval($activity->start_time, $activity->end_time);
            $activity->duration = $duration;
            if($activity->save()){
                $person->last_checked_in=$activity->end_time;

                $error = false;
                $message = $person->name . " has been successfully checked out of " . $room->name . " at " . $activity->end_time;
            }else{
                $error = false;
                $message = "Error when recording activity in database";
            }
        }else{
            $activity->end_time = date('Y-m-d H:i:s', strtotime(date("Y-m-d", strtotime($activity->start_time))." 23:59:00"));
            $result = $this->CheckIn($person, $room);
            $error = $result["error"];
            $message = $result["message"];
        }

        return array("error"=>$error, "message"=>$message);
    }
}

