<?php

class SessionController extends ControllerBase
{
    public function initialize(){
        $this->tag->setTitle('Log In/Sign Up');
        parent::initialize();
    }

    public function indexAction(){

    }

    private function _registerSession(Staff $staff){
        $person = Person::findFirst(array("id_of_person = \"$staff->id_of_person\""));
        if($person){
            $this->session->set('authentication', array(
                "id_of_staff" => $staff->id_of_staff,
                "id_of_person" =>$staff->id_of_person,
                "name" => $person->name,
                "RFID" => $person->RFID,
                "isAdmin"=> $staff->is_admin
            ));
        }
    }

    public function startAction(){
        if($this->request->isPost()){
            $email = $this->request->getPost('email');
            $password = $this->request->getPost('password');

            $staff = Staff::findFirst(array("email = :email:", "bind"=>array("email"=>$email)));
            if($staff){
                $encryptedPassword = base64_encode(sha1($password . $staff->salt, true) . $staff->salt);
                if($encryptedPassword == $staff->encrypted_password){
                    $this->_registerSession($staff);
                    $this->flash->success("Welcome " . $this->session->get("authentication")["name"]);
                    return $this->forward("dashboard/index");
                }
            }
            $this->flash->error("Combination is not correct");
            return $this->forward("session/index");
        }
    }

    public function endAction(){
        $this->session->remove('authentication');
        $this->flash->success("Goodbye!");
        return $this->forward("index/index");
    }
}