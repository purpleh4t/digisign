<?php
use Phalcon\Mvc\View;

class SignageController extends ControllerBase
{
    public function initialize(){
        $this->tag->setTitle("Signage");
        parent::initialize();
    }

    public function indexAction(){
        $this->view->roomElements = new RoomElements();
    }

    public function showAction(){
        $room = Room::findFirst(array("id_of_room = :id_of_room:", "bind"=>array("id_of_room"=>$this->request->getQuery("room"))));
        if($room){
            $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
            $this->view->roomName = "Electronic Systems Laboratory and Common Room";
            $this->view->departmentName = "Department of Electrical Engineering and Information Technology";


        }else{
            $this->forward("signage/index");
            $this->flash->error("Room can't be found");
        }
    }
}

