<?php

class RoomsController extends ControllerBase
{
    public function initialize(){
        $this->tag->setTitle("Rooms Management");
        parent::initialize();
        parent::mustLogin();
    }

    public function indexAction(){
        $this->view->roomElements = new RoomElements();
        $this->view->controllerName =  $this->router->getControllerName();
    }

    public function showAction(){
        if($this->request->hasQuery("room")){
            $room = Room::findFirst(array("id_of_room = :id_of_room:", "bind"=>array("id_of_room"=>$this->request->getQuery("room"))));
            $this->view->title = $room->name;

            $session = $this->session->get("authentication");
            if($session["isAdmin"] == true){
                $this->view->isValidToAdd = true;
            }else{
                $this->view->isValidToAdd = false;
            }
            $this->view->session_id_of_person = $session["id_of_person"];

            $staffData = Staff::find(array("id_of_room = :id_of_room:", "bind"=>array("id_of_room"=>$this->request->getQuery("room")), "order"=>"room_order ASC"));
            $lecturers = array();
            $staffs = array();
            $guests = array();
            foreach ($staffData as $staff){
                $temp = array();
                $person = Person::findFirst(array("id_of_person = '$staff->id_of_person'"));
                $last_update = $person->last_checked_in;
                if($last_update == null){
                    $last_update = "Never Checked In";
                }
                $status = "Hasn't Checked In Today";
                if(parent::isToday($person->last_checked_in)){
                    $activity = Activity::findFirst(array("id_of_person = \"$person->id_of_person\" AND id_of_room = $room->id_of_room", "order"=>"start_time DESC"));
                    if($activity->duration=="0"){
                        $status = "IN";
                    }else{
                        $status = "OUT";
                    }
                }

                $temp["id_of_person"] = $staff->id_of_person;
                $temp["name"] = $person->name;
                $temp["type"] = $person->type;
                $temp["email"] = $staff->email;
                $temp["status"] = $status;
                $temp["last_update"] = $last_update;
                $temp["room_order"] = $staff->room_order;
                if($person->type == "Lecturer"){
                    $lecturers[] = (object)$temp;
                }else{
                    $staffs[] = (object)$temp;
                }
            }
            $this->view->lecturers = $lecturers;
            $this->view->staffs = $staffs;
            $this->view->guests = $guests;
            $this->view->id_of_room = $room->id_of_room;
        }else{
            $this->flash->error("A room must be selected");
            $this->forward('rooms/index');
        }
    }

    public function addAction(){
        if($this->request->isGet()){
            if($this->request->hasQuery("id_of_room") && ($this->request->getQuery("type") == "Lecturer" || $this->request->getQuery("type") == "Staff")){
                $room = Room::findFirst(array("id_of_room = :id_of_room:", "bind"=>array("id_of_room"=>$this->request->getQuery("id_of_room"))));
                if($room){
                    $type = $this->request->getQuery("type");
                    $this->view->title = "Add a " . $type . " at " . $room->name;

                    $entity["id_of_room"]=$room->id_of_room;
                    $entity["type"]=$type;
                    $this->view->searchStaffForm = new SearchStaffForm(null, $entity);

                    $staffs = array();
                    if($this->request->hasQuery("keywords")){
                        $keywords = $this->request->getQuery("keywords", "string");
                        if($keywords == ""){
                            $this->flash->warning("Please don't leave keywords blank");
                        }else{
                            $people = Person::find(array("(id_of_person LIKE :keywords: OR name LIKE :keywords:) AND type = '$type'", "bind"=>array("keywords"=>'%'.$keywords.'%')));
                            if(count($people)==0){
                                $this->flash->warning("There is no data with keywords of '" . $keywords ."'");
                            }
                            foreach ($people as $person){
                                $staff = Staff::findFirst(array("id_of_person = '$person->id_of_person'"));
                                $roomStaff = Room::findFirst(array("id_of_room = '$staff->id_of_room'"));
                                if($staff->id_of_room != $room->id_of_room){
                                    $temp = array(
                                        "id_of_person"=>$person->id_of_person,
                                        "name"=>$person->name,
                                        "current_room"=>$roomStaff->name
                                    );
                                    $staffs[] = (object)$temp;
                                }
                            }

                            // Adding Form in Modal
                            $options = array(
                                "id_of_room"=>$room->id_of_room,
                                "name"=>$room->name
                            );
                            $moveStaffForm = new MoveStaffForm(null, $options);

                            $this->view->moveStaffForm = $moveStaffForm;
                        }
                    }
                    $this->view->staffs = $staffs;
                }else{
                    $this->flash->warning("Room ID is not exist");
                    $this->forward("rooms/index");
                }
            }else{
                $this->flash->warning("Please choose a room first");
                $this->forward("rooms/index");
            }
        }elseif($this->request->isPost()){
            $this->forward("rooms/index");
            $id_of_room = $this->request->getPost("id_of_room");
            $id_of_person = $this->request->getPost("id_of_person");

            $person = Person::findFirst(array("id_of_person=\"$id_of_person\""));
            $room = Room::findFirst(array("id_of_room = :id_of_room:", "bind"=>array("id_of_room"=>$id_of_room)));
            if($person && $room){
                $director = Director::findFirst(array("id_of_person = \"$person->id_of_person\""));
                if($director){
                    $directors = Director::find(array("id_of_room = $room->id_of_room"));
                    $highestOrderRoom = 0;
                    foreach ($directors as $Director){
                        if($highestOrderRoom<=$Director->room_order){
                            $highestOrderRoom = $Director->room_order;
                        }
                    }
                    $director->room_order = $highestOrderRoom + 1;
                    $director->id_of_room = $room->id_of_room;
                    $director->save();
                    $this->flash->success($person->name . " has been successfully moved to " . $room->name);
                }else{
                    $staff = Staff::findFirst(array("id_of_person = \"$person->id_of_person\""));
                    if($staff){
                        $staffs = Staff::find(array("id_of_room = $room->id_of_room"));
                        $highestOrderRoom = 0;
                        foreach ($staffs as $Staff){
                            if($highestOrderRoom<=$Staff->room_order){
                                $highestOrderRoom = $Staff->room_order;
                            }
                        }
                        $staff->room_order = $highestOrderRoom + 1;
                        $staff->id_of_room = $room->id_of_room;
                        $staff->save();
                    }
                    $this->flash->success($person->name . " has been successfully moved to " . $room->name);
                }
            }else{
                $this->flash->error("The submitted data is not valid");
            }
        }
    }

    public function modifyAction(){
        $session = $this->session->get("authentication");
        $this->forward("rooms/index");
        if($this->request->hasPost("id_of_room") && $this->request->hasPost("id_of_person") && $this->request->getPost("action") == "increase" && $session["isAdmin"]){
            $idOfPerson = $this->request->getPost("id_of_person");
            $idOfRoom = $this->request->getPost("id_of_room", "int");

            // Increase Room Order
            $submitted_director = Director::findFirst(array("id_of_person = :id_of_person: AND id_of_room = :id_of_room:", "bind"=>array("id_of_person"=>$idOfPerson, "id_of_room"=>$idOfRoom)));
            if($submitted_director){
                $submitted_director->room_order--;
                $submitted_director->save();
                $conflictedOrderDirector = Director::findFirst(array("id_of_room = $submitted_director->id_of_room AND room_order = $submitted_director->room_order AND id_of_person != \"$submitted_director->id_of_person\""));
                if($conflictedOrderDirector){
                    $conflictedOrderDirector++;
                    $conflictedOrderDirector->save();
                }

                $message = $idOfPerson . " :: " . $idOfRoom . " :: " . "director increase to :: " . $submitted_director->room_order;
                $this->flash->success($message);
            }else{
                $submitted_staff = Staff::findFirst(array("id_of_person = :id_of_person: AND id_of_room = :id_of_room:", "bind"=>array("id_of_person"=>$idOfPerson, "id_of_room"=>$idOfRoom)));
                if($submitted_staff){
                    $submitted_staff->room_order--;
                    $submitted_staff->save();
                    $conflictedOrderStaff = Staff::findFirst(array("id_of_room = $submitted_staff->id_of_room AND room_order = $submitted_staff->room_order AND id_of_person != \"$submitted_staff->id_of_person\""));
                    if($conflictedOrderStaff){
                        $conflictedOrderStaff->room_order++;
                        $conflictedOrderStaff->save();
                    }

                    $message = $idOfPerson . " :: " . $idOfRoom . " :: " . "staff increase to :: " . $submitted_staff->room_order;
                    $this->flash->success($message);
                }
            }
        }
        // Decrease Room Order
        elseif ($this->request->hasPost("id_of_room") && $this->request->hasPost("id_of_person") && $this->request->getPost("action") == "decrease" && $session["isAdmin"]){
            $idOfPerson = $this->request->getPost("id_of_person");
            $idOfRoom = $this->request->getPost("id_of_room", "int");

            $submitted_director = Director::findFirst(array("id_of_person = :id_of_person: AND id_of_room = :id_of_room:", "bind"=>array("id_of_person"=>$idOfPerson, "id_of_room"=>$idOfRoom)));
            if($submitted_director){
                $submitted_director->room_order++;
                $submitted_director->save();
                $conflictedOrderDirector = Director::findFirst(array("id_of_room = $submitted_director->id_of_room AND room_order = $submitted_director->room_order AND id_of_person != \"$submitted_director->id_of_person\""));
                if($conflictedOrderDirector){
                    $conflictedOrderDirector--;
                    $conflictedOrderDirector->save();
                }

                $message = $idOfPerson . " :: " . $idOfRoom . " :: " . "director decrease to :: " . $submitted_director->room_order;
                $this->flash->success($message);
            }else{
                $submitted_staff = Staff::findFirst(array("id_of_person = :id_of_person: AND id_of_room = :id_of_room:", "bind"=>array("id_of_person"=>$idOfPerson, "id_of_room"=>$idOfRoom)));
                if($submitted_staff){
                    $submitted_staff->room_order++;
                    $submitted_staff->save();
                    $conflictedOrderStaff = Staff::findFirst(array("id_of_room = $submitted_staff->id_of_room AND room_order = $submitted_staff->room_order AND id_of_person != \"$submitted_staff->id_of_person\""));
                    if($conflictedOrderStaff){
                        $conflictedOrderStaff->room_order--;
                        $conflictedOrderStaff->save();
                    }

                    $message = $idOfPerson . " :: " . $idOfRoom . " :: " . "staff decrease to :: " . $submitted_staff->room_order;
                    $this->flash->success($message);
                }
            }
        }
        else{
            $this->flash->error("The submitted data is not valid");
        }
    }
}

