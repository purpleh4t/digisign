<?php

class NewsController extends ControllerBase
{
    public function initialize(){
        $this->tag->setTitle("Manage News");
        parent::initialize();
        parent::mustLogin();
    }

    public function indexAction(){
        $this->view->roomElements = new RoomElements();
    }

    public function showAction(){
        if($this->request->isGet()){
            if($this->request->hasQuery("room")){
                $room = Room::findFirst(array("id_of_room = :id_of_room:", "bind"=>array("id_of_room"=>$this->request->getQuery("room"))));
                if($room){
                    $this->view->title = $room->name;
                    $this->view->room = $room->id_of_room;
                    $id_of_person = $this->session->get("authentication")["id_of_person"];
                    $this->view->session_id_of_person = $id_of_person;

                    if($room->id_of_room == Staff::findFirst(array("id_of_person = \"$id_of_person\""))->id_of_room){
                        $this->view->validToAdd = true;
                    }else{
                        $this->view->validToAdd = false;
                    }

                    $this->removeExpiredNewsActives();
                    $newsActives = NewsActive::find(array("id_of_room = $room->id_of_room"));
                    $news = array();
                    foreach ($newsActives as $newsActive){
                        $person = Person::findFirst(array("id_of_person = \"$newsActive->author\""));
                        if($person){
                            $news[] = (object)array(
                                "id_of_news"=>$newsActive->id_of_news,
                                "author"=>$person->name,
                                "id_of_person"=>$newsActive->author,
                                "title"=>$newsActive->title,
                                "content"=>$newsActive->content,
                                "startTime"=>$newsActive->start_time,
                                "endTime"=>$newsActive->end_time,
                                "imageLink"=>$newsActive->image_link
                            );
                        }
                    }
                    $this->view->news = $news;
                    $this->view->editNewsForm = new AddNewsForm(null, array("id_of_room"=>$room->id_of_room));

                    $newsLogs = null;
                    if($this->request->hasQuery("dateFrom") && $this->request->hasQuery("dateTo")){
                        $this->flash->success("Search outdated news from : " . $this->request->getQuery("dateFrom") . ", to : " . $this->request->getQuery("dateTo"));
                        $startTime = $this->request->getQuery("dateFrom");
                        $endTime = $this->request->getQuery("dateTo");
                        $startTime = date("Y-m-d H:i:s", strtotime($startTime));
                        $endTime = date("Y-m-d H:i:s", strtotime($endTime));
                        $newsRecords = NewsLog::find(array("(
                            (start_time <= \"$startTime\" AND end_time >= \"$startTime\") OR
                            (start_time >= \"$startTime\" AND end_time <= \"$endTime\") OR
                            (start_time <= \"$endTime\" AND end_time >= \"$endTime\") OR
                            (start_time <= \"$startTime\" AND end_time >= \"$endTime\")
                            ) AND id_of_room = \"$room->id_of_room\""));
                        $newsLogs = array();
                        if(count($newsRecords)>0){
                            foreach ($newsRecords as $newsRecord){
                                $person = Person::findFirst(array("id_of_person = \"$newsRecord->author\""));
                                if($person){
                                    $newsLogs[] = (object)array(
                                        "id_of_news"=>$newsRecord->id_of_news,
                                        "author"=>$person->name,
                                        "id_of_person"=>$newsRecord->author,
                                        "title"=>$newsRecord->title,
                                        "content"=>$newsRecord->content,
                                        "startTime"=>$newsRecord->start_time,
                                        "endTime"=>$newsRecord->end_time,
                                        "imageLink"=>$newsRecord->image_link
                                    );
                                }
                            }
                        }
                    }
                    $this->view->newsLogs = $newsLogs;
                }else{
                    $this->flash->error("There is no data");
                    $this->forward("news/index");
                }
            }

        }else{
            $this->flash->error("A room must be selected");
            $this->forward('news/index');
        }

        $dateForm = new SearchNewsDateForm();
        $this->view->dateForm = $dateForm;
    }

    public function addAction(){
        if($this->request->isGet() && $this->request->hasQuery("room")){
            $room = Room::findFirst(array("id_of_room = :id_of_room:", "bind"=>array("id_of_room"=>$this->request->getQuery("room", "alphanum"))));
            if($room){
                $this->flash->warning("This is applied for " . $room->name);
                $this->view->form = new AddNewsForm(null, array("id_of_room"=>$room->id_of_room));
            }else{
                $this->flash->error("There is no data");
                $this->forward("news/index");
            }
        }elseif($this->request->isPost() && $this->request->hasPost("title")){
            $title = $this->request->getPost("title", "string");
            $content = $this->request->getPost("content", "string");
            $image = $this->request->getPost("image");
            $startTime = $this->request->getPost("startTime");
            $endTime = $this->request->getPost("endTime") . " 23:59:59";
            $id_of_person = $this->session->get("authentication")["id_of_person"];

            $person = Person::findFirst(array("id_of_person = \"$id_of_person\""));
            $room = Room::findFirst(array("id_of_room = :id_of_room:", "bind"=>array("id_of_room"=>$this->request->getPost("room"))));
            // Filter data must contain title and content and valid dates today and  end dates always must valid
            if($person && $room && $title != null && $content != null && (date("Ymd") <= date("Ymd", strtotime($startTime))) && (date("Ymd", strtotime($endTime)) >= date("Ymd", strtotime($startTime)))){
                $news = new NewsLog();
                $news->id_of_room = $room->id_of_room;
                $news->title = $title;
                $news->content = $content;
                $news->image_link = " ";
                $news->start_time = date("Y-m-d H:i:s", strtotime($startTime));
                $news->end_time = date("Y-m-d H:i:s", strtotime($endTime));
                $news->author = $id_of_person;

                if($news->create()){
                    $newsActive = new NewsActive();
                    $newsActive->id_of_news = $news->id_of_news;
                    $newsActive->id_of_room = $news->id_of_room;
                    $newsActive->title = $news->title;
                    $newsActive->content = $news->content;
                    $newsActive->image_link = $news->image_link;
                    $newsActive->start_time = $news->start_time;
                    $newsActive->end_time = $news->end_time;
                    $newsActive->author = $news->author;
                    if($newsActive->create()){
                        $this->flash->notice("Success adding news with title \"".$news->title."\" at ".$room->name);
                    }else{
                        $this->flash->error("Error when adding news to database");
                    }
                }else{
                    $this->flash->error("Error when adding news to database");
                }
            }else{
                $this->flash->error("Data submitted is not valid. Please check the submission data is complete and start time must starts from today.");
            }
            $this->forward("news/index");
        }else{
            $this->flash->error("A room must be selected");
            $this->forward("news/index");
        }
    }

    public function editAction(){
        if ($this->request->isPost() && $this->request->hasPost("idOfNews") &&
            $this->request->hasPost("title") && $this->request->hasPost("content") &&
            $this->request->hasPost("startTime") && $this->request->hasPost("endTime")){
            $id_of_person = $this->session->get("authentication")["id_of_person"];
            $id_of_news = $this->request->getPost("idOfNews");
            $title = $this->request->getPost("title");
            $content = $this->request->getPost("content");
            $start_time = date("Y-m-d H:i:s", strtotime($this->request->getPost("startTime")));
            $end_time = date("Y-m-d H:i:s", strtotime($this->request->getPost("endTime")));
            $news = NewsLog::findFirst(array("id_of_news = :id_of_news: AND author = :id_of_person:", "bind"=>array("id_of_news"=>$id_of_news, "id_of_person"=>$id_of_person)));
            if($news && (date("Ymd", strtotime($end_time)) >= date("Ymd", strtotime($start_time)))){
                $news->title = $title;
                $news->content = $content;
                $news->start_time = $start_time;
                $news->end_time = $end_time;
                $room = Room::findFirst(array("id_of_room = $news->id_of_room"));

                $newsActive = NewsActive::findFirst(array("id_of_news = $news->id_of_news"));
                if($newsActive){
                    $newsActive->title = $news->title;
                    $newsActive->content = $news->content;
                    $newsActive->start_time = $news->start_time;
                    $newsActive->end_time = $news->end_time;
                    $newsActive->save();
                }

                if($room && $news->save()){
                    $this->flash->success("Success updating news with title \"".$news->title."\" at ".$room->name);
                }else{
                    $this->flash->error("Error when updating news to database");
                }
            }else{
                $this->flash->error("Submitted data is not found");
            }
        }else{
            $this->flash->error("Submitted data is not valid");
        }
        $this->forward("news/index");
    }

    public function deleteAction(){
        if($this->request->isPost() && $this->request->hasPost("idOfNews") &&
            $this->request->hasPost("title") && $this->request->hasPost("content") &&
            $this->request->hasPost("startTime") && $this->request->hasPost("endTime")){
            $id_of_person = $this->session->get("authentication")["id_of_person"];
            $id_of_news = $this->request->getPost("idOfNews");

            $news = NewsLog::findFirst(array("id_of_news = :id_of_news: AND author = :id_of_person:", "bind"=>array("id_of_news"=>$id_of_news, "id_of_person"=>$id_of_person)));
            if($news){
                $newsActive = NewsActive::findFirst(array("id_of_news = $news->id_of_news"));
                $room = Room::findFirst(array("id_of_room = $news->id_of_room"));
                if($newsActive){
                    $newsActive->delete();
                }
                if($room && $news->delete()){
                    $this->flash->success("Success deleting news with title \"".$news->title."\" at ".$room->name);
                    $this->forward("news/index");
                }else {
                    $this->flash->error("Error when deleting news from database");
                    $this->forward("news/index");
                }
            }
        }else{
            $this->flash->error("Submitted data is not valid");
            $this->forward("news/index");
        }
    }

    protected function removeExpiredNewsActives(){
        $expiredNewsActives = NewsActive::find(array("start_time > NOW() OR end_time < NOW()"));
        if(count($expiredNewsActives)>0){
            $this->flash->warning("There is ". count($expiredNewsActives) ." expired news");
            foreach ($expiredNewsActives as $expiredNewsActive){
                $expiredNewsActive->delete();
            }
        }
    }
}

