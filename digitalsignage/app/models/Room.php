<?php

class Room extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id_of_room;

    /**
     *
     * @var string
     */
    public $id_of_building;

    /**
     *
     * @var string
     */
    public $id_of_xbee;

    /**
     *
     * @var string
     */
    public $code_room;

    /**
     *
     * @var integer
     */
    public $floor;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $description;

    /**
     *
     * @var string
     */
    public $image_link;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'room';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Room[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Room
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
