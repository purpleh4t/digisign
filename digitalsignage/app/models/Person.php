<?php

class Person extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    public $id_of_person;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $type;

    /**
     *
     * @var string
     */
    public $image_link;

    /**
     *
     * @var string
     */
    public $RFID;

    /**
     *
     * @var string
     */
    public $last_checked_in;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'person';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Person[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Person
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
