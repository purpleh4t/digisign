<?php

class Building extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    public $id_of_building;

    /**
     *
     * @var string
     */
    public $id_of_faculty;

    /**
     *
     * @var string
     */
    public $id_of_department;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $description;

    /**
     *
     * @var string
     */
    public $latitude;

    /**
     *
     * @var string
     */
    public $longitude;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'building';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Building[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Building
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
