<?php

/**
 * Created by PhpStorm.
 * User: HermawanRahmatHidaya
 * Date: 19/09/2016
 * Time: 22.20
 */

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Forms\Element\File;
use Phalcon\Forms\Element\Date;

class AddNewsForm extends Form{
    public function initialize($entity = null, $options = null){
        $title = new Text('title');
        $title->setLabel('Title');
        $this->add($title);

        $content = new TextArea('content');
        $content->setLabel("Content");
        $this->add($content);

        $image = new File("image");
        $image->setLabel("Image");
        $this->add($image);

        $startTime = new Date("startTime");
        $startTime->setLabel("Start Time");
        $this->add($startTime);

        $endTime = new Date("endTime");
        $endTime->setLabel("End Time");
        $this->add($endTime);

        $id_of_room = new \Phalcon\Forms\Element\Hidden("room");
        $id_of_room->setDefault($options["id_of_room"]);
        $this->add($id_of_room);
    }
}