<?php

/**
 * Created by PhpStorm.
 * User: HermawanRahmatHidaya
 * Date: 30/09/2016
 * Time: 23.01
 */

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Hidden;
use \Phalcon\Forms\Element\Select;
class MoveStaffForm extends Form{
    public function initialize($entity = null, $options = array()){
        $changeStaffForm = new \Phalcon\Forms\Form();
        $staffIDText = new \Phalcon\Forms\Element\Text("id_of_person");
        $staffIDText->setLabel("ID");
        $this->add($staffIDText);

        $staffIDName = new \Phalcon\Forms\Element\Text("name");
        $staffIDName->setLabel("Name");
        $this->add($staffIDName);

        $fromRoomName = new \Phalcon\Forms\Element\Text("from_name");
        $fromRoomName->setLabel("From");
        $fromRoomName->setDefault(null);
        $this->add($fromRoomName);

        $toRoomID = new \Phalcon\Forms\Element\Hidden("id_of_room");
        $toRoomID->setDefault($options["id_of_room"]);
        $this->add($toRoomID);

        $toRoomName = new \Phalcon\Forms\Element\Text("to_name");
        $toRoomName->setLabel("To");
        $toRoomName->setDefault($options["name"]);
        $this->add($toRoomName);
    }
}